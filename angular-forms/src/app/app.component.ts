import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import {MyValidators} from './my.validators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  form: FormGroup;

  ngOnInit(){
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.email,
         Validators.required,
          MyValidators.restrictedEnails
         ],
        MyValidators.uniqEmail),
      password: new FormControl(null, [ Validators.minLength(7), Validators.required ]),
      address:  new FormGroup({
        country: new FormControl('ua'),
        city: new FormControl('', Validators.required)
      }),
      skills: new FormArray([])
    })
  }

  submit(){
    console.log('Form submitted', this.form);
    const formData = {...this.form.value};
    console.log('formData', formData);

    this.form.reset();
  }

  setCapital(){
    const cityMap = {
      ru: 'Москва',
      ua: 'Киев',
      by: 'Минск'
    }
    const cityKey = this.form.get('address').get('country').value;
    const city = cityMap[cityKey];

    this.form.patchValue({ address: { city: city } });
  }

  addSkill(){
    const control = new FormControl('', Validators.required);
    // <FormArray>this.form.get('skills').push()
    (this.form.get('skills') as FormArray).push( control );
  }

}
