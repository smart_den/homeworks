import { compute } from './compute';

describe('compute', () => { 

    it('should return 0 if negative input', () => {
        // передаем -1
        const result = compute(-1)
        // ожилаем получить 0
        expect(result).toBe(0)
    })

    it('should imcrement input if positive ', () => {
        // передаем 1
        const result = compute(1)
        // ожилаем получить 2
        expect(result).toBe(2)
    })

})