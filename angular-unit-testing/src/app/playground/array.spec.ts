import { countries } from './array';

describe("countries", () => {
  it("should contaim counties codes", () => {
      const result = countries();

      expect(result).toContain('ru')
      expect(result).toContain('ua')
      expect(result).toContain('uk')
  });
});