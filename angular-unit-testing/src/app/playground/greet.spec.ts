import { greet } from './greet';

describe('greet', () => {
    it('should include name in return message', () => {
        // toBe = должно быть полное соответствие 
        /* expect(greet('Denis')).toBe('Hello Denis'); */
        // toContain - не строгое. Проято должна входить
        expect(greet('Denis')).toContain('Denis');
    })
} )
