import { PostsComponent } from "./posts.component";
import { PostsService } from './posts.service';
import { EMPTY, throwError } from 'rxjs';
import { of } from 'rxjs';

describe('PostsComponent', () => {

  let component: PostsComponent;
  let service: PostsService;

  beforeEach( () => {
    service = new PostsService(null);
    component = new PostsComponent(service);
  })

  // протестим у сервиса метод fetch и что загрузка пошла
  it('it should call fetch when ngOnInit', () => {
    // spyOn - метод шпионит за вызовами сервиса 
    const spy = spyOn(service, 'fetch').and.callFake( () => {
      return EMPTY;
    });

    component.ngOnInit()

    expect(spy).toHaveBeenCalled()
  });


  // проверим на данные 
  it('it should update posts lenght after ngOnInit', () => {
    const posts = [1, 2, 3, 4];
    // spyOn - метод шпионит за вызовами сервиса
    spyOn(service, "fetch").and.returnValue( of(posts) ) ;

    component.ngOnInit();

    expect(component.posts.length).toBe(posts.length);
  })

  // проверим метод add
  it('it should add new post', () => {

  const post = {title: 'test'}
  const spy = spyOn(service, 'create').and.returnValue(of(post));

  component.add(post.title);

  expect(spy).toHaveBeenCalled();
  expect(component.posts.includes(post)).toBeTruthy();

  });


  // проверим на ошибку
 // tslint:disable-next-line: align
 it('it should catch error', () => {
  const error = 'Error message'
  spyOn(service, 'create').and.returnValue(throwError(error));

  component.add('Post Title');

  expect(component.message).toBe(error)

 });

 // delete
  it('it should remove post if user confirms', () => {

    const spy = spyOn(service, 'remove').and.returnValue(EMPTY);

    spyOn(window, "confirm").and.returnValue(true);

    component.delete(10);

    expect(spy).toHaveBeenCalledWith(10);
 });

 // cancel delete
  it('it should*not remove post if user doesnt confirm', () => {

    const spy = spyOn(service, 'remove').and.returnValue(EMPTY);

    spyOn(window, "confirm").and.returnValue(false);

    component.delete(10);

    expect(spy).not.toHaveBeenCalled();
 });



})
