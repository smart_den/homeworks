import { CounterComponent } from './counter.component';
import { FormBuilder } from '@angular/forms';

// Пропустить весь тест  xdescribe
describe('CounterComponent', () => {

      let component: CounterComponent;
      // beforeEach - выполнить какоето действие перед каждым it
      //              например будем создавать компонент как новый instance
      // beforeAll -  бедет вызван перед всеми it
      // afterEach - будет вызван после завершения каждого it 
      // afterAll - будет вызван после всех it 
      beforeEach( () => {
        component = new CounterComponent(new FormBuilder());
      });

      it('should uncrement counter by 1', () => {
          component.increment();
          expect(component.counter).toBe(1);
      });

      // пропустить пункт теста  Пишем xit
      xit('should decrement counter by 1', () => {
          component.decrement();
          expect(component.counter).toBe(-1);
      });

      it('should decrement counter by 1', () => {
          component.decrement();
          expect(component.counter).toBe(-1);
      });

      it("should increment value by event emitter", () => {
        let result = null;
        component.counterEmitter.subscribe( value => {
          result = value;
        });
        component.increment();
        expect(result).toBe(1);
      });

      //form

      // Протестируем что контролы были созданы

      it('should creat form with 2 controls', () => {
         expect(component.form.contains('login')).toBe(true)
         expect(component.form.contains('email')).toBeTruthy()
      });

      // тест на работу валидаторов

      it("should mark login as invalid if empty value", () => {

        const control = component.form.get('login');

        control.setValue('');

        expect(control.valid).toBeFalsy();
        
       });

});
