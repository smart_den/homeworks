import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthInterceptor implements HttpInterceptor {
  // intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  //   return next.handle(req);
  // }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const cloned = req.clone( {
      headers: req.headers.append('Auth', 'Some Random Token')
    } );

    return next.handle(cloned).pipe(
        tap( event => {
          if (ebent,type === HttpEventType.Response ){
            console.log('Console Interseptor response', event);
          }
        })
    );
  }
}
