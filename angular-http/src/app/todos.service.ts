import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {  map, catchError, tap } from 'rxjs/operators';
import {delay} from 'rxjs/operators';


// Интерфейс лучше держать в сервисе
export interface Todo {
  completed: boolean;
  title: string;
  id?: number;
}


@Injectable({
  // регистрация в корневом модуле
  providedIn: 'root'
})
export class TodosService {

  constructor( private http: HttpClient ) { }


  addTodo( todo: Todo ): Observable<Todo>{
    // Инициализируем стрим
    const headers = new HttpHeaders().append('MyOwnHeader', 'Denis cool');

    return this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', todo , {headers: headers})

  }

  fetchTodos(): Observable<Todo[]>{

    let params = new HttpParams().set('_limit', '5').append('custom', 'anything')

    return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos', {params: params , observe: 'response' })
    .pipe(
      map(response =>{
        // console.log(response)
        return response.body;
      }),
      delay(1500))
    .pipe( catchError( error => {
      //  console.log('ERROR', error.message )
       return  throwError(error)
      })
    )
  }

  removeTodo(id: number): Observable<any>{
    return  this.http.delete<void>( `https://jsonplaceholder.typicode.com/todos/${id}`, {observe: 'events'} ).pipe(
      tap( event => {
        // console.log( event )
        if ( event.type === HttpEventType.Sent){
          console.log('SENT', event)
        }
        if ( event.type === HttpEventType.Response){
          console.log('Response', event)
        }

      })
    )
  }

  completeTodo(id: number): Observable<Todo>{
    return  this.http.put<Todo>(`https://jsonplaceholder.typicode.com/todos/${id}`, { completed: true}, {responseType: 'json'} )
  }


}
