import { Component, OnInit } from '@angular/core';
import {Todo, TodosService} from './todos.service';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  todos: Todo[] = []

  todoTitle = '';

  loading = false

  error: string = '';

  constructor( private todosService: TodosService ){

  }

  ngOnInit(){
    this.fetchTodos()
  }


  addTodo(){
    // если строка состоит только из пробелов
    if( !this.todoTitle.trim() ){
      return
    }

    const newTodo: Todo = {
      title: this.todoTitle,
      completed: false
    }

    this.todosService.addTodo(newTodo).subscribe( res => {
      this.todos.push(res)
      this.todoTitle = '';
    })

  }


  fetchTodos(){
    this.loading = true;
    this.todosService.fetchTodos().subscribe( res => {
      this.todos = res
      this.loading = false
    }, error => { console.log(error); this.error = error.message ; } )
  }


  removeTodo( id: number){
    this.todosService.removeTodo(id).subscribe( ()=>{
      this.todos = this.todos.filter( t =>  t.id != id )
    })
  }

  completeTodo(id: number){
    this.todosService.completeTodo(id).subscribe( res => {
     this.todos.find( t => t.id === id ).completed = true
    })
  }


}
