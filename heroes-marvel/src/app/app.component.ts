import { Component, ViewContainerRef, OnInit, ViewChild } from "@angular/core";
import { ComponentFactoryResolver } from "@angular/core";
import { MapComponent } from "./map/map.component";
import { CounterComponent } from "./counter/counter.component";

export interface Heroes {
  id: number;
  name: string;
  power: number;
  image: string;
}

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  title = "heroes-marvel";

  @ViewChild(CounterComponent)
  private counterComponent: CounterComponent;

  public heroesList: Heroes[] = [
    { id: 1, name: "Iron Man", power: 90, image: "assets/img/ironman.png" },
    {
      id: 2,
      name: "Captain America",
      power: 80,
      image: "assets/img/captainamerica.png"
    }
  ];

  constructor(
    private viewContainerRef: ViewContainerRef, // куда мы будем подгружать наш компонент
    private componentFactoryresolver: ComponentFactoryResolver
  ) {}

  ngOnInit() {}

  increment() {
    this.counterComponent.increment();
  }
  decrement() {
    this.counterComponent.decrement();
  }

  // Динамически по клику добавить новый компонент на страницу
  addToMap() {
    // фабрика куда передаем компонент который хотим вставить
    const componentFactory = this.componentFactoryresolver.resolveComponentFactory(
      MapComponent
    );
    // получим ссылку на компонент
    const componentRef = this.viewContainerRef.createComponent(
      componentFactory
    );
  }
}
