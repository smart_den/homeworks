import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.sass']
})
export class TimerComponent implements OnInit {

  public timer = 100;
  public interval;

  constructor() { }

  ngOnInit() {
  }

  reset() {
    this.timer = 100;

    clearInterval(this.interval);
  }

  play() {
    this.interval = setInterval(
      _ => {
        this.timer-- ;
      }
    , 1000);
  }


}
