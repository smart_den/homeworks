import { Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.sass']
})
export class CounterComponent implements OnInit {


  public counter = 100;

  constructor() { }

  ngOnInit() {
  }


  increment() { this.counter++; }
  decrement() { this.counter--; }



}
