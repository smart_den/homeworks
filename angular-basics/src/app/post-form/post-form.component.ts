import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Post } from '../app.component';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.sass']
})
export class PostFormComponent implements OnInit {

  @Output() onAdd: EventEmitter<Post> = new EventEmitter<Post>();
  @Output() onTitleChanged: EventEmitter<Post> = new EventEmitter<Post>();

  //обратимся к элементу на котором стоит  #titleInput
  @ViewChild('titleInput', {static: false}) inputRef: ElementRef;

  @ViewChild('formContainer', {static: false}) myDivRef: ElementRef;

  @ViewChild('borderInit', {static: true}) myBorderRef: ElementRef;

  title = '';
  text  = '';

  constructor() { }

  ngOnInit() {
    this.myBorderRef.nativeElement.style.border = "1px solid green";
  }

  addPost(){
    if (this.title.trim() && this.text.trim()) {
        const post: Post = {
          title: this.title,
          text: this.text
        };

        this.onAdd.emit(post);
        this.onTitleChanged.emit(post);

        // console.log( 'New Post', post );

        this.title = this.text = '';
    }
  }

  focusTitle(){
    // console.log( 'New Post', this.inputRef );
    console.log( 'New Post', this.myDivRef );
    this.inputRef.nativeElement.focus();
    this.myDivRef.nativeElement.style.border = "1px solid red";
  }

}
