import { Component, OnInit, Input, ContentChild, ElementRef, OnChanges, SimpleChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Post } from '../app.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class PostComponent implements
      OnChanges,
      OnInit,
      DoCheck,
      AfterContentInit,
      AfterContentChecked,
      AfterViewInit,
      AfterViewChecked,
      OnDestroy
  {

  @Input() post: Post;

  @Output() onRemove = new EventEmitter<number>();

  @ContentChild('info', {static: true})  infoRef: ElementRef;

  constructor() {
    // console.log('constructor')
  }

  removePost(){
      this.onRemove.emit(this.post.id)
  }

  ngOnInit() {
    // console.log('nndnnd', this.infoRef.nativeElement)
    console.log('ngOnInit')
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges', changes)
  }
  ngDoCheck() {
    console.log('ngDoCheck')
  }
  ngAfterContentInit() {
    console.log('AfterContentInit')
  }
  ngAfterContentChecked() {
    console.log('AfterContentChecked')
  }
  ngAfterViewInit() {
    console.log('AfterViewInit')
  }
  ngAfterViewChecked() {
    console.log('AfterViewInit')
  }
  ngOnDestroy() {
    console.log('OnDestroy')
  }

}
