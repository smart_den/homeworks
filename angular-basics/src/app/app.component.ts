import { Component } from '@angular/core';

export interface Post {
  title: string;
  text: string;
  id?: number;
}

export interface myCar {
  id: number;
  name: string;
  type: string;
  color: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  bigTitle = '';
  posts: Post[] = [
    {title: 'Angular components', text: 'lorem lorem lorem', id: 1},
    {title: 'Angular components2', text: 'lorem2 lorem2 lorem2', id: 2},
  ];

  carList: myCar[] = [
    { id: 1, name: 'Mercedes', type: 'hatchback', color: 'silver'},
    { id: 2, name: 'Bmw', type: 'hatchback', color: 'red'},
    { id: 3, name: 'Audi', type: 'hatchback', color: 'blue'},
  ];

  updatePosts(post: Post){
   /*  console.log('postpost', post) */
    this.posts.unshift(post)
  }

  updateTitle(post: Post){
    this.bigTitle = 'Добавлен новый пост';
    setTimeout( _=>{
      this.bigTitle = '';
    }, 3000)
  }

  removePost(id: number){
    // console.log(id)
    this.posts = this.posts.filter( p => p.id !== id)
  }
}
