import { Component, OnInit, Input } from '@angular/core';
import { myCar } from '../app.component';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.sass']
})
export class CarsComponent implements OnInit {

  @Input() cars: myCar;

  constructor() { }

  ngOnInit() {
  }

}
