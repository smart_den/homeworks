import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  public x = 5;

  public user = {name: 'Denis', age: 33};

  public myClass = 'red';

  public myColor = 'blue';

  public textColor = '';

  public clickOnChild = '';

  constructor() {
    setTimeout(()=>{
      this.myClass = 'green';
      setTimeout(() => {
        this.myClass = 'blue';
      }, 2000);
    }, 2000);
   }

  ngOnInit() {
  }


  changeColourClick(){
    this.myColor = 'red';
  }

  changeSmth(color: string){
    this.textColor = color;
  }

  myAlert(text){
    alert(text);
  }

}
