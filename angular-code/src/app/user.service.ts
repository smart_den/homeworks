import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users = [
    {name: 'Den'},
    {name: 'Max'},
    {name: 'John'}
  ] ;


  constructor(private http: HttpClient) { }

  public getAll() {
    return this.users;
  }

  public remove(name: string) {
    return this.users = this.users.filter(user => user.name !== name);
  }

  public add(name: string) {
    this.users.push({ name  } );
  }

  public sendRequest(){
     return this.http.get('https://jsonplaceholder.typicode.com/users')
  }

}
