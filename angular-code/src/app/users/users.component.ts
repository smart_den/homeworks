import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router, Event, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass']
})
export class UsersComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router) {
      this.route.queryParams.subscribe(params => console.log(params));
      this.router.events.subscribe((event: Event) => {
        // console.log(event) ;
        // выполнение действий если произошло событе navigation Start
        if(event instanceof NavigationStart){
          // console.log('Navigation starttt') ;
        }
      })
  }

  ngOnInit() {
  }

}
