import { TestBed, fakeAsync, flush } from '@angular/core/testing';

import { CalculatorService } from './calculator.service';

describe('CalculatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CalculatorService = TestBed.get(CalculatorService);
    expect(service).toBeTruthy();
  });

  it('should return sum', () => {
    const service: CalculatorService = TestBed.get(CalculatorService);
    expect(service.sum(2, 3)).toBeTruthy(5);
  });

  it('should return async sum', fakeAsync(() => {
    const service: CalculatorService = TestBed.get(CalculatorService);
    service.sumAsync(2, 3).then(result => {
      expect(result).toBeTruthy(5);
    });
    flush();
  }));

});
