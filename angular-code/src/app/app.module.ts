import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { UserCardComponent } from './header/user-card/user-card.component';
import { ItemComponent } from './item/item.component';
import { ColoryDirective } from './colory.directive';
import { DelayDirective } from './delay.directive';
import { SomeItemComponent } from './some-item/some-item.component';
import { UserService } from './user.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyinterceptorService } from './myinterceptor.service';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';
import { UsersComponent } from './users/users.component';
import { ProfileComponent } from './user/profile/profile.component';
import { SettingsComponent } from './user/settings/settings.component';
import { AuthGuard } from './auth.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReactiveFormsComponent } from './reactive-forms/reactive-forms.component';


/*
   С помощью хуков остеживаются события до перехода на страницу

 - CanActivate/CanActivateChild  - может ли роутер активировать данный стейт
 - CanDeactivate
 - CanLoad
 - Resolve

*/

const routes = [
  { path: '', component: HomeComponent },
  { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
  { path: 'about', component: AboutComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'reactive-forms', component: ReactiveFormsComponent },
  { path: 'users',
      canActivate: [ AuthGuard ],
      component: UsersComponent
  },
  { path: 'users/:userId', component: UserComponent , children: [
    { path: 'profile', component: ProfileComponent },
    { path: 'settings', component: SettingsComponent}
  ]}
];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    UserCardComponent,
    ItemComponent,
    ColoryDirective,
    DelayDirective,
    SomeItemComponent,
    HomeComponent,
    AboutComponent,
    ContactsComponent,
    UsersComponent,
    ProfileComponent,
    SettingsComponent,
    ReactiveFormsComponent
  ],
  entryComponents: [ SomeItemComponent ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes , {preloadingStrategy: PreloadAllModules}),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    UserService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyinterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
