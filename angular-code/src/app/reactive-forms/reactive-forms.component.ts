import { Component, OnInit } from '@angular/core';
import { FormControl , Validators, FormGroup, FormArray, FormBuilder} from '@angular/forms';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.sass']
})
export class ReactiveFormsComponent implements OnInit {

  nameControl: FormControl;
  fullNameControl: FormGroup;
  userListControl: FormGroup;

  constructor( private formDuilder: FormBuilder) { }

  ngOnInit() {
    this.nameControl = new FormControl('John', [Validators.required, Validators.minLength(5), myValidator ], [myAsyncValidator] );
    // отслеживаем изменение инпута
    this.nameControl.valueChanges.subscribe( (val) => { console.log(val)} );
    // отслеживаем изменение статуса  ( Валидна ли форма  VALID)
    this.nameControl.statusChanges.subscribe( (status) => {
      console.log(this.nameControl.errors );
      console.log(status);
    } );

    this.fullNameControl = new FormGroup({
        firstName: new FormControl(),
        lastName: new FormControl()
    });
    this.fullNameControl.valueChanges.subscribe( (val) => { console.log(val)} );

    // this.userListControl =  new FormGroup({
    //       users:  new FormArray([
    //                     new FormControl('Alice'),
    //                     new FormControl('Den'),
    //                     new FormControl('Max')
    //                   ])
    // });

    this.userListControl = this.formDuilder.group({
      users: this.formDuilder.array([ ['Alice'], ['Den'], ['Max'] ])
    })
    this.userListControl.valueChanges.subscribe( (val) => { console.log(val)} );
  }
  addUserControl(){
    (this.userListControl.controls['users'] as FormArray).push( new FormControl(''));
  }

  removeUserControl(index: number){
    (this.userListControl.controls['users'] as FormArray).removeAt(index);
  }

}




function myValidator( formControl: FormControl){
  if( formControl.value.length < 3  ){
    // http.get
      return {  myValidator: { message: 'My value should be longer then 3'} };
  } else {
      return null;
  }
}

function myAsyncValidator( formControl: FormControl): Observable<null|any>{
  if( formControl.value.length < 3  ){
      return of({ myValidator: { message: 'My value should be longer then 3'} });
  } else {
      return of(null);
  }
}
