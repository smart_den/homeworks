import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
/* import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch'; */
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MyinterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
    const request  = req.clone({params : req.params.set( 'x', '5' )});

    return next.handle(request);
/*     return next.handle(request).catch( error => {
      if(error.status === 401 ){
        console.log('Redirect to login')
      }
      return Observable.throw(error);
    }); */
  }
}
