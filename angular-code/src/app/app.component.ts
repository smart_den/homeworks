import { Component, ViewContainerRef, TemplateRef, ComponentFactoryResolver, OnInit } from '@angular/core';
import { SomeItemComponent } from './some-item/some-item.component';
import { UserService } from './user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit{
  title = 'angular-code';
  public users;

  constructor(
    private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private userService: UserService
  ) { }

  ngOnInit() {

    this.users = this.userService.getAll();
    console.log(this.users);

    setTimeout( _ => {
      const componentFactory =  this.componentFactoryResolver.resolveComponentFactory( SomeItemComponent );
      const componentRef = this.viewContainerRef.createComponent(componentFactory);
    }, 8000 );


    this.userService.sendRequest().subscribe(data => {
      this.users = data;
    });


  }

  removeItem(name: string){
    this.userService.remove(name);
    this.users = this.userService.getAll();
  }

  addUser(name: string){
    if(!name){
      return ;
    }
    this.userService.add(name);
    this.users = this.userService.getAll();
  }




}
