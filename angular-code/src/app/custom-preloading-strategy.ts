import { PreloadingStrategy, Route } from '@angular/router';
import { Observable, of} from 'rxjs';


export class CustomPreloadingStrategy implements PreloadingStrategy {
  preload(route: Route, load: () => Observable<any>): Observable<any>{
     // return of(null);   // если не хотим делать предзагрузку
     return load();
    //  of(true).delay(5000).do( _=>{
    //    load();
    //  })
  }
}
