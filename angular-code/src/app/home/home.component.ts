import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToUser(userId){
    /* this.router.navigate(['users', userId]); */
    this.router.navigateByUrl('users/' + userId);
    this.router.navigateByUrl('users/' + userId, {skipLocationChange: true});

  }

}
