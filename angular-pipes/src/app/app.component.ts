import { Component } from '@angular/core';

export interface Post {
  title: string;
  text: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  e: number = Math.E;

  obj: object = { i: 1, name: 'Den' };

  search: string | number = '' ;

  posts: Post[] = [
    {title: 'Beer', text: 'Самое лучшее пиво в мире!'},
    {title: 'Bread', text: 'The best bread in the world'},
    {title: 'Water', text: 'The best water in the world'}
  ];
}
