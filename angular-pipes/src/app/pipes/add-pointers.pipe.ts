import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "addPointers"
})
export class AddPointersPipe implements PipeTransform {
  transform(str: string): string {
    if (str.length > 7) {
      return `${str.slice(0, 7)}...`;
    } else {
      return str;
    }
  }
}
