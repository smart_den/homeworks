import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StyleDitrective } from './directives/style.directive';
import { FormsModule } from '@angular/forms';
import { TestDirectiveDirective } from './directives/test-directive.directive';
import { BigFontDirective } from './directives/big-font.directive';
import { IfnotDirective } from './directives/ifnot.directive';

@NgModule({
  declarations: [
    AppComponent,
    StyleDitrective,
    TestDirectiveDirective,
    BigFontDirective,
    IfnotDirective
  ],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
