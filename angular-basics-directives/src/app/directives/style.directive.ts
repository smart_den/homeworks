import { Directive, ElementRef, Renderer2, HostListener, Input, HostBinding } from '@angular/core';

@Directive({
  selector: '[appStyle]'
})

export class StyleDitrective{
  constructor(private el: ElementRef, private renderer: Renderer2) {
    // console.log(el)
    const span = this.renderer.createElement('span');
    const text = this.renderer.createText('Renderer2 lib');
    this.renderer.appendChild(span, text);


    el.nativeElement.style.color = 'blue';
    this.renderer.addClass(this.el.nativeElement, 'vvv')
    this.renderer.appendChild(this.el.nativeElement, span);


    this.renderer.setStyle(this.el.nativeElement, 'color', 'green' )

  }



  @Input('appStyle') color: string = 'blue';
  @Input() fontWeight: string = 'normal';
  @Input() dStyles: { border?: string, borderRadius?: string, padding?: string }


  @HostListener('mouseover', ['$event.target']) onMouseover(event: Event){
    this.renderer.setStyle(this.el.nativeElement, 'color', this.color)
    this.renderer.setStyle(this.el.nativeElement, 'fontWeight', this.fontWeight)
    this.renderer.setStyle(this.el.nativeElement, 'border', this.dStyles.border)
    this.renderer.setStyle(this.el.nativeElement, 'borderRadius', this.dStyles.borderRadius)
    this.renderer.setStyle(this.el.nativeElement, 'padding', this.dStyles.padding + 'px')
  }
  @HostListener('mouseleave', ['$event.target']) onCMouseleave(event: Event){
    this.renderer.setStyle(this.el.nativeElement, 'color', 'green')
    this.renderer.setStyle(this.el.nativeElement, 'fontWeight', 'normal')
    this.renderer.setStyle(this.el.nativeElement, 'border', 'none')
    this.renderer.setStyle(this.el.nativeElement, 'borderRadius', '0')
    this.renderer.setStyle(this.el.nativeElement, 'padding', '0px')
  }

  @HostBinding('style.background') elBack = 'null';
  @HostBinding('class.myBorder') myBord: boolean = false;

  @HostListener('click', ['$event.target']) onClick(event: Event){
   this.elBack = 'lightblue';
   this.myBord = true;
  }



}
