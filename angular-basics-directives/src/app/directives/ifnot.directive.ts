import { Directive, ViewContainerRef, TemplateRef, Input } from "@angular/core";

@Directive({
  selector: "[appIfnot]"
})
export class IfnotDirective {
  // сеттер
  @Input("appIfnot") set ifNot(condition: boolean) {
    if (!condition) {
      // показать элементы
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      //скрыть
      this.viewContainer.clear();
    }
  }

  //templateRef - содержимое ng-template
  //viewContainer - указывает ng-template

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}
}
