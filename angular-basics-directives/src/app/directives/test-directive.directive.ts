import { Directive, ElementRef, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[appTestDirective]'
})
export class TestDirectiveDirective implements OnInit {

  constructor(private el: ElementRef, private r: Renderer2) { }

  ngOnInit(){

    this.r.addClass(this.el.nativeElement, 'wild');

    this.r.setStyle(
      this.el.nativeElement,
      'border',
      '2px solid olive'
    );
    this.r.setStyle(
      this.el.nativeElement,
      'margin',
      '0 auto'
    );

    this.r.setProperty(this.el.nativeElement, 'value', 'Cute alligator');


  }

}
