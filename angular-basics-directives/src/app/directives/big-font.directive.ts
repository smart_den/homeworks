import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[BigFontDirective]'
})
export class BigFontDirective {

  constructor(private el: ElementRef, private r: Renderer2) { }

  @HostListener('click', ['$event.target'])  onClick(event: Event){
    this.r.setStyle(this.el.nativeElement, 'font-size', '50px');
  }

}
