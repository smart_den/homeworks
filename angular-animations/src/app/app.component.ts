import { Component } from '@angular/core';
import {
  trigger,
  state,
  style,
  transition,
  animate,
  group,
  keyframes,
} from "@angular/animations";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  animations: [
    trigger("mybox", [
      state("start", style({ background: "blue" })),
      state("end", style({ background: "red", transform: "scale(1.2)" })),
      state(
        "special",
        style({
          background: "orange",
          transform: "scale(0.5)",
          borderRadius: "50%"
        })
      ),
      transition("start => end", animate(450)),
      transition("end => start", animate("800ms ease-in-out")),
      /*       transition('special <=> *', animate(500)), */
      transition("special <=> *", [
        style({ background: "green" }),
        animate("1s", style({ background: "pink" })),
        animate(750)
      ]),
      // для плавного появления скрытия
      // "void => *
      transition(":enter", [
        /*  style({ opacity: 0 }),
        animate("850ms ease-out") */
        animate(
          "3s",
          keyframes([
            style({ background: "red", offset: 0 }),
            style({ background: "orange", offset: 0.3 }),
            style({ background: "green", offset: 0.6 }),
            style({ background: "blue", offset: 1 })
          ])
        )
      ]),
      // * => void
      transition(":leave", [
        style({ opacity: 1 }),
        group([
          animate(750, style({ opacity: 0, transform: "scale(1.2)" })),
          animate(300, style({ color: "lime", fontWeight: "bold" }))
        ])
      ])
    ])
  ]
})
export class AppComponent {
  // переменная состояния нужного нам элемента
  boxState = "start";
  visible = true;

  animate() {
    this.boxState = this.boxState === "start" ? "end" : "start";
  }
}
