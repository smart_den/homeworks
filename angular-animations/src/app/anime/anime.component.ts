import { Component, OnInit } from '@angular/core';
import { trigger, transition, useAnimation } from "@angular/animations";
import { bounce, jello } from "ng-animate";

@Component({
  selector: 'app-anime',
  templateUrl: './anime.component.html',
  styleUrls: ['./anime.component.scss'],
  animations: [
    trigger('bounce',
     [transition('void => *', useAnimation(bounce)),
     transition('* => void', useAnimation(jello)),
    ])
  ]
})
export class AnimeComponent implements OnInit {
  visible = true;

  constructor() { }

  ngOnInit() {
  }

}
