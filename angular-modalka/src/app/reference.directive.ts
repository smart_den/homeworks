import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
  selector: "[appRef]"
})
export class ReferenceDirective {
  constructor(public containerRef: ViewContainerRef) {}
}
