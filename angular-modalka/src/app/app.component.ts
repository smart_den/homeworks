import { Component, ComponentFactoryResolver, ViewChild } from "@angular/core";
import { ModalComponent } from "./modal/modal.component";
import { ReferenceDirective } from "./reference.directive";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  @ViewChild(ReferenceDirective, { static: false }) refDir: ReferenceDirective;

  /**
   * @param resolver;
   */
  constructor(private resolver: ComponentFactoryResolver) {}

  showModal() {
    // Фабрика которая создает компонент ModalComponent
    const modalFactory = this.resolver.resolveComponentFactory(ModalComponent);
    // Для отображения нового созданного компонента на странице используем
    // очистка контейнера
    this.refDir.containerRef.clear();
    const component = this.refDir.containerRef.createComponent(modalFactory);

    component.instance.textec = 333;

    component.instance.close.subscribe(() => {
      this.refDir.containerRef.clear();
    });
  }
}
