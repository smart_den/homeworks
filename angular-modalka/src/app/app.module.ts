import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ModalComponent } from './modal/modal.component';
import { ReferenceDirective } from './reference.directive';


@NgModule({
  declarations: [AppComponent, ModalComponent, ReferenceDirective],
  imports: [BrowserModule],
  providers: [],
  entryComponents: [ModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
