import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"]
})
export class ModalComponent implements OnInit {
  @Input() textec = 123456;
  @Output() close = new EventEmitter<void>();

  constructor() {}

  ngOnInit() {
    this.textec = new Date().getTime();
  }
}
