import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostService } from '../../shared/post.service';
import { Post } from '../../shared/interfaces';
import { Subscription } from 'rxjs';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit, OnDestroy {
  posts: Post[] = [];
  // для очистки подписки чтобы избежать утечек памяти
  postsSubscribtion: Subscription;
  deleteSubscribtion: Subscription;
  searchStr = '';

  constructor(
    private postService: PostService,
    private alertService: AlertService
    ) {}

  ngOnInit() {
    this.postsSubscribtion = this.postService.getAll().subscribe(res => {
      this.posts = res;
    });
  }

  remove(id: string) {
   this.deleteSubscribtion = this.postService.remove(id).subscribe(() => {
     this.posts = this.posts.filter(post => {
       return post.id !== id;
     });
     this.alertService.warning('Пост был удален');
   });
  }

  ngOnDestroy() {
    // так мы избежим утечек памяти
    if (this.postsSubscribtion) {
      this.postsSubscribtion.unsubscribe();
    }
    if (this.deleteSubscribtion) {
      this.deleteSubscribtion.unsubscribe();
    }
  }
}
