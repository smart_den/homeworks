import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError, Subject } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { FirebaseAuthResponse, User } from '../../../shared/interfaces';

@Injectable({providedIn: 'root'})
export class AuthService {
  // заведем публичную переменную для ошибок и обозначим что это стрим с помощью $
  // Subject - это обсервабл в котором мы можем эмитить события
  public error$: Subject<string> = new Subject<string>();

  constructor(private http: HttpClient) {}

  // Получене токена и проверка на время жизни токена
  get token(): string {
    const expDate = new Date(localStorage.getItem('fb-token-exp'));
    // проверка на время жизни токена
    if (new Date() > expDate) {
      this.logout();
      return null;
    }
    return localStorage.getItem('fb-token');
  }

  login(user: User): Observable<any> {
    user.returnSecureToken = true;
    return this.http
      .post(
        `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`,
        user
      )
      .pipe(tap(this.setToken), catchError(this.handleError.bind(this)));
  }

  // приватный метод для отлова ошибок
  private handleError(error: HttpErrorResponse) {
    const { message } = error.error.error;

    switch (message) {
      case 'INVALID_EMAIL':
        // эмитим новое событие-зеачение
        this.error$.next('Неверный email');
        break;
      case 'INVALID_PASSWORD':
        this.error$.next('Неверный пароль');
        break;
      case 'EMAIL_NOT_FOUND':
        this.error$.next('Email не найден');
        break;
    }

    return throwError(error);
  }

  logout() {
    this.setToken(null);
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  // сохранение токена и время жизни в локал сторедж
  private setToken(response: FirebaseAuthResponse | null) {
    // в случае если почта и пароль верны Firebase вернет нам
    /*
      displayName: ""
      email: "denismaslogm@gmail.com"
      expiresIn: "3600"
      idToken: "eyJhbGciOiJSUzI1NiIsImtpZGb2J5h5kUw"
      kind: "identitytoolkit#VerifyPasswordResponse"
      localId: "6JkBxO2vW3cdb6A7joIBvT40l7u1"
      refreshToken: "AEu4IL2RqNLIiSldvjaYzkZ3vCWXE"
      registered: true
    */
    console.log(response);
    if (response) {
      const expDate = new Date(
        new Date().getTime() + +response.expiresIn * 1000
      );
      localStorage.setItem('fb-token', response.idToken);
      localStorage.setItem('fb-token-exp', expDate.toString());
    } else {
      localStorage.clear();
    }
  }
}


