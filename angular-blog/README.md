1. Обработка ошибок

    В сервисе мы делаем запрос кудато и получаем стрим

    import { Observable, throwError, Subject } from 'rxjs';
    import { tap, catchError } from 'rxjs/operators';
    import { HttpClient, HttpErrorResponse } from '@angular/common/http';

      login(user: User): Observable<any> {
        user.returnSecureToken = true;
        return this.http
        .post(
            `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`,
            user
        )
        .pipe(tap(this.setToken), catchError(this.handleError.bind(this)));
    }

    ловим ошибки через catchError

    // приватный метод для отлова ошибок
    private handleError(error: HttpErrorResponse) {
        const { message } = error.error.error;

        switch (message) {
        case 'INVALID_EMAIL':
            // эмитим новое событие-зеачение
            this.error$.next('Неверный email');
            break;
        case 'INVALID_PASSWORD':
            this.error$.next('Неверный пароль');
            break;
        case 'EMAIL_NOT_FOUND':
            this.error$.next('Email не найден');
            break;
        }

        return throwError(error);
    }

    // заведем публичную переменную для ошибок и обозначим что это стрим с помощью $
    // Subject - это обсервабл в котором мы можем эмитить события

    import { Observable, throwError, Subject } from 'rxjs';

    public error$: Subject<string> = new Subject<string>();

    Далее auth.service у нас заинжекчен в логин компонент

        export class LoginPageComponent implements OnInit {

        constructor(
            public auth: AuthService,
        ) {}

    Тоесть в самой верстке мы уже выведем

    <div class="alert alert-danger" *ngIf="auth.error$ | async as error" >
        {{ error }}
    </div>

2. Добавление гварда

    Чтобы не попадали на закрытые страницы

    В папке с сервисами создадим гвард. По сути это тоже сервис

        import { Injectable } from "@angular/core";

        @Injectable()
        export class AuthGuard{
            
        }

    И сразу зарегаем его в админском модуле

          providers: [AuthService, AuthGuard]


    Все
    полный код

    import { Injectable } from '@angular/core';
    import { CanActivate, Router, UrlTree, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
    import { Observable } from 'rxjs';
    import { AuthService } from './auth.service';


    @Injectable()
    export class AuthGuard implements CanActivate {

        constructor(
            private auth: AuthService,
            private router: Router
            ) {}

        canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot
        ): Observable<boolean> | Promise<boolean> | boolean {
        if (this.auth.isAuthenticated()) {
            return true;
        } else {
            this.auth.logout();
            this.router.navigate(['/admin', 'login' ], {
                queryParams: {
                    loginAgain: true
                }
            });
        }
      }
        
    }

    теперь опишем его в роутере и все


    RouterModule.forChild([
      {
        path: '',
        component: AdminLayoutComponent,
        children: [
          { path: '', redirectTo: '/admin/login', pathMatch: 'full' },
          { path: 'login', component: LoginPageComponent },
          { path: 'dashboard', component: DashboardPageComponent, canActivate: [AuthGuard] }, <--
          { path: 'create', component: CreatePageComponent , canActivate: [AuthGuard]},      <--
          { path: 'post/:id/edit', component: EditPageComponent , canActivate: [AuthGuard] } <--
        ]
      }


3. Подключение текстового редактора

    На npmjs.com найдем ngx-quill

       npm i ngx-quill

    далее установить просто quill

       npm i quill

    for projects using Angular < v5.0.0 install npm install ngx-quill@1.6.0

    install @angular/core, @angular/common, @angular/forms, @angular/platform-browser, quill and rxjs - peer dependencies of ngx-quill

    include theme stylings: bubble.css, snow.css of quilljs in your index.html, or add them in your css/scss files with @import statements, or add them external stylings in your build process.

    For standard webpack, angular-cli and tsc builds

    import QuillModule from ngx-quill:

        import { QuillModule } from 'ngx-quill'

    add QuillModule to the imports of your NgModule:

        @NgModule({
        imports: [
            ...,

            QuillModule.forRoot()
        ],
        ...
        })
        class YourModule { ... }

    В нашем случае зарегаем в глобальном shared.module

    @NgModule({
        imports: [
            HttpClientModule,
            QuillModule.forRoot()
        ],
        exports: [
            HttpClientModule,
            QuillModule
        ]
    })

    В верстке добавим 

    <quill-editor formControlName="text"></quill-editor>

    Подключим css

    В файле styles.scss

    @import "~quill/dist/quill.core.css";
    @import "~quill/dist/quill.snow.css";


4. Хранение данных в БД firebase

    Зайдем в консоль Firebase

    Раздел Database

    Найдем Realtime Database

    Запустить в защищенном режиме

    Настроить правила

        раздел правила

            {
                "rules": {
                    ".read": true,
                    ".write": "auth != null"
                }
            }

            Опубликовать !!!

    Скопируем предоставленный урл и вставим в наш сервис. туда юудем отправлять запросы

    Только сделаем по правилам

    В папке environments

        interfaces.ts

            export interface Environment {
                apiKey: string;
                production: boolean;
                fbDbUrl: string;  <---------  firebase database url
            }

        environment.ts

            import { Environment } from './interface';

            export const environment: Environment = {
                production: false,
                apiKey: 'AIzaSyCn39QZWMq9CKiAShsj8JldcXxkqcAVmP0',
                fbDbUrl: 'https://angular-blog-d6679.firebaseio.com'   <---  firebase database url
            };

        environment.prod.ts

            import { Environment } from './interface';

            export const environment: Environment = {
                production: false,
                apiKey: 'AIzaSyCn39QZWMq9CKiAShsj8JldcXxkqcAVmP0',
                fbDbUrl: 'https://angular-blog-d6679.firebaseio.com'   <---  firebase database url
            };


5. Создадим интерцептор для обработки ошибок с сервера

    > auth.interceptor.ts


    import { Injectable } from '@angular/core';
    import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
    import { Observable, throwError } from 'rxjs';
    import { AuthService } from '../admin/shared/services/auth.service';
    import { Router } from '@angular/router';
    import { catchError } from 'rxjs/operators';

    @Injectable()
    export class AuthInterceptor implements HttpInterceptor {

        constructor(
            private auth: AuthService,
            private router: Router
        ) {

        }

        intercept(req: HttpRequest<any>,  next: HttpHandler): Observable<HttpEvent<any>> {

            // если пользователь авторизован то для каждого запроса добавляем токен
            if (this.auth.isAuthenticated()) {
                // клонируем запрос чтобы его переопределить и отправить в firebase
                req = req.clone({
                    setParams: {
                        auth: this.auth.token
                    }
                });
            }

            return next.handle(req)
                .pipe(
                    catchError( (error: HttpErrorResponse) => {
                        console.log('InterceptorError', error);
                        if (error.status === 401) {
                            this.auth.logout();
                            this.router.navigate( ['/admin', 'login'], {
                                queryParams: {
                                    authFailed: true
                                }
                            });
                        }
                        // throwError - делает из ошибки Observable
                        return throwError(error);
                    })
                );
        }
    }



    интерсепторы всегда регают в главном модуле app.module

        const INTERCEPTOR_PROVIDER: Provider = {
            provide: HTTP_INTERCEPTORS,
            // multi: true - for interceptors dont erase each other
            multi: true,
        useClass: AuthInterceptor
        };


        @NgModule({
        declarations: [
            AppComponent,
            MainLayoutComponent,
            HomePageComponent,
            PostPageComponent,
            NotfoundPageComponent,
            PostComponent
        ],
        imports: [
            BrowserModule,
            AppRoutingModule,
            // общий модуль для подключения http и в админке и на главной
            SharedModule
        ],
        providers: [INTERCEPTOR_PROVIDER],   <------------
        bootstrap: [AppComponent]
        })

6. PIPE поиска в списке , фильтр

    >html
    <input type="text" placeholder="Find post..." [(ngModel)]="searchStr">
    > component
    searchStr = '';

    создаем пайп

    >admin/shared/search.pipe.ts

    import { Pipe, PipeTransform } from '@angular/core';
    import { Post } from '../../shared/interfaces';

    @Pipe({
        name: 'searchPosts'
    })
    export class SearchPipe implements PipeTransform {
        transform( posts: Post[], search = '' ): Post[] {
            if (!search.trim()) {
                return posts;
            }

            return posts.filter( post => {
                return post.title.toLocaleLowerCase().includes(search.toLowerCase());
            } );
        }
    }

    > Зарегаем пайп в админском модуле
    import { SearchPipe } from "./shared/search.pipe";
        @NgModule({
        declarations: [
            SearchPipe
        ],

    >теперь в верстке

      <tr *ngFor="let post of posts | searchPosts: searchStr; let idx = index">


7. Компонент АЛЕРТА

    Сделаем его в виде компонента

    ng g c admin/shared/components/alert --skipTests

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    <div class="alert-wrap" *ngIf="text">
    <div 
    class="alert"
    [ngClass]="{
        'alert-success': type === 'success',
        'alert-warning': type === 'warning',
        'alert-danger': type === 'danger'
    }"
    >
        <p>{{text}}</p>
    </div>
    </div>  
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    .alert-wrap {
        position: fixed;
        top: 50px;
        right: 40px;
        max-width: 200px;
    }
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  создадим сервис alert.service.ts

  зарегаем в модуле в провайдерах 

   providers: [ AuthGuard, AlertService]


   >alert.service.ts

   import { Injectable } from '@angular/core';
    import { Subject } from 'rxjs';

    export type AlertType = 'success' | 'warning' | 'danger';

    export interface Alert {
    type: AlertType;
    text: string;
    }

    @Injectable()
    export class AlertService {
    // стрим
    public alert$ = new Subject<Alert>();

    success(text: string) {
        this.alert$.next({ type: 'success', text });
    }

    warning(text: string) {
        this.alert$.next({ type: 'warning', text });
    }

    danger(text: string) {
        this.alert$.next({ type: 'danger', text });
    }

    }


    > alert.component.ts

    
    import { Component, OnInit, Input, OnDestroy } from '@angular/core';
    import { AlertService } from '../../services/alert.service';
    import { Subscription } from 'rxjs';

    @Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss']
    })
    export class AlertComponent implements OnInit, OnDestroy {

    // сколько времени визуально отображать данный алерт
    @Input() delay = 5000;

    public text: string;
    public type = 'success';

    alertSub: Subscription;

    constructor( private alertService: AlertService) { }

    ngOnInit() {
        // подпишемся на стрим алерт сервиса
        this.alertSub = this.alertService.alert$.subscribe( alert => {
            this.text = alert.text;
            this.type = alert.type;
        });

        const timeout = setTimeout( () => {
            clearTimeout(timeout);
            this.text = '';
        }, this.delay );
    }

    ngOnDestroy() {
        if (this.alertSub) {
        this.alertSub.unsubscribe();
        }
    }

    }


    Тепереь разместим алерт в админском лэйауте в верстке

    <app-alert></app-alert>


    теперь в компоненте создания постов

    constructor(
        private postsService: PostService,
        private alertService: AlertService
    ) { }

     this.postsService.create(post).subscribe( (res) => {
       console.log('res', res);
       this.form.reset();
       this.alertService.success('Пост был создан')  <---------------
    });



