import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {

  showIds = false;

  constructor( private postsService: PostsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe( (params: Params) => {
      console.log(params);
      this.showIds = !!params.showIds;
    })
  }


  showIdsProgremm(){
      this.router.navigate(['/posts'], { queryParams: {showIds: true}, fragment: 'count=2'  } );
  }

}
