cd ..# HoMeWoRkS

---

1. Установка при помощи NVM

---

    curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh -o install_nvm.sh

    bash install_nvm.sh

    source ~/.profile

    nvm ls-remote

    nvm install 8.11.1

    nvm use 8.11.1

    node -v

    nvm alias default 8.11.1

---

2. запуск приложения с 12 нодой

---

    ng serve      or    sudo ng serve

---

3. Динамические компоненты

---

    - In app.module.ts need add

         entryComponents: [ MapComponent],

---

4. Tooltips

---

    npm i ng2-tooltip-directive --save

in app.module
import { TooltipModule } from 'ng2-tooltip-directive';

     imports: [
        TooltipModule
    ],

    <span tooltip="Tooltip" placement="top" show-delay="500">Tooltip on top</span>

---

5.  Установка ANGULAR CLI angular command line interface

---

        npm i -g @angular/cli

---

6.  Создать новый проект

---

        ng new angular-doc

---

7. Запуск проекта

---

        ng serve

---

8. Cоздать новый компонент

---

    ng generate component user

---

9. Вывод данных на экран

---

    строковая интерполяция

            {{name}}

---

10. Динамические свойства

---

        в компоненте
             public myClass = 'red';
        в шаблоне
            <span [class]="myClass">{{ 'Hello, ' + user.name + ' yours age '+ user.age}}</span>
        итог
            текст красного цвета


        Через 2 сек поменять цвет

             constructor() {
                setTimeout(()=>{
                this.myClass = 'green';
                    setTimeout(() => {
                        this.myClass = 'blue';
                    }, 2000);
                }, 2000);
            }

    По аналогии

        [style]
        [src]
        [alt]
        [href]

    [style.color] = "'red'"
    [style.width.px] = "100"

---

11. События

    (click)="changeColor()"

    (change)="changeSmth($event.target.value)"

    (input)="changeSmth($event.target.value)"

    (keydown.enter)="changeColourClick()"

    (keydown.control.shift.enter)="changeColourClick()"


    <input type="text" placeholder="color of p" (input)="changeSmth($event.target.value)" >

---

12. Работа с элементами по ссылке

    <input type="text" #myInput>
    <button (click)="myAlert(myInput.value)">My btn</button>


    Используется ссылка #

---

13. Вложенные компоненты

    Создадим компонент user-card внутри другого компонента header
    ng g c header/user-card


        Передача данных во вложенный шаблон

            <app-user-card [user]="user"></app-user-card>

        в компоненте вложенного шаблона принимаем эту переменную

            import { Input } from '@angular/core';

            @Input() user;

---

14. Директива *ngIf *ngFor

    <ul>
        <li *ngFor="let item of [1,2,3]; let index = $index">{{item}}</li>
    </ul>

---

15. Передача событий из дочернего компонента в родительский


    В родительском компоненете создаем свое собственное событие childClicked
    и слушаем его и вешаем на ребенка

    <app-user-card (childClicked)="clickOnChild='Кликнули по ребенку'"></app-user-card>

    В родительской компоненте соответственно зададим переменную

    public clickOnChild = '';


    далее в ребенке в html отследим событие клика по ребенку

    <p (click)="selectUser()">user-card works!</p>

    А в компоненте ребенка

    import { Output, EventEmitter } from '@angular/core';

    // eventEmitter - для генерации событий
     @Output() childClicked: EventEmitter<any> = new EventEmitter();

      selectUser() {
            this.childClicked.emit();
            //this.childClicked.emit(user);
        }

---

16. Проекция контента с помощью ng-content


      В родительской компоненте верстке вставляем свой контент внутрь ребенка

      <app-item>
          <h1>Привет из родительского компонента!!!</h1>
      </app-item>


      А внутри верстки ребенка используем ng-content чтобы отобразить то что передал родитель

      <p>item works!</p>
      <ng-content #name></ng-content>




      также можно указать по частям что куда подгружать

      <app-item>
        <h1>Привет из родительского компонента!!!</h1>
        <p>this is the P</p>
      </app-item>


      <ng-content select="p"></ng-content>
      <p>item works!</p>
      <ng-content select="h1"></ng-content>

---

17. Атрибут директива


        Команда создания директивы

            ng generate directive colory

            ng g d colory

    В корне создался файл colory.directive.ts

    В верстке какому-то элементу дадим атрибут app-colory и к нему применится наша директива

    <div appColory></div>
                            import { Directive, HostBinding, HostListener } from '@angular/core';

                            @Directive({
                            selector: '[appColory]'
                            })
                            export class ColoryDirective {

                                @HostBinding('style.color') myColor: string;
                                @HostListener('click', ['event']) changeColor(event){
                                    this.myColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
                                };

                                constructor() {

                                    this.myColor = 'orange';
                                    setTimeout(_=>{
                                    this.myColor = 'lime';
                                    },2000)
                                }

                            }

---

18. STRUCTURES DIRECTIVES

Lets create direwctive wich will be add some compopnent on our page after timing delay

    ng g d delay

_appDelay - _-its mean structure
structure directive remove our element from DOM

In html we add

<div *appDelay="3000">Directive with delay</div>

3000 - delay

In directive we add

        import { Directive, ViewContainerRef, TemplateRef, OnInit, Input } from '@angular/core';

        @Directive({
        selector: '[appDelay]'
        })
        export class DelayDirective  implements OnInit{

        @Input() appDelay: number;

        constructor(
            private template: TemplateRef<any> ,
            private view: ViewContainerRef
        ) { }

        ngOnInit(){
            setTimeout(_ => {
                this.view.createEmbeddedView(this.template);
            }, this.appDelay);
        }

        }

<!-- every secons add new component on the page -->
<div *ngFor="let delay of [1000,2000,3000,4000,5000,6000,7000]">
    <app-item *appDelay="delay"></app-item>
</div>

---

19. Export as


    @Directive({
        selector: '[appColory]'
        exportAs:  'coloryyyy'
    })



    in html


    <div appColory #colory1="coloryyyy"> Some text </div>
    <div appColory #colory2="coloryyyy"> Some text </div>
    <button (click)="colory1.setRandomColory(); colory2.setRandomColory();">Change color</button>

    We have change color on both elements

---

20. Dynamical components - we show it after some delay or event

    Мы не указываем их в щаблоне но потом динамически добавляем

    ng g c some-item

    В app.module обязательно указать что наш комплнент будет подгружаться динамически
    добавим entryComponents

            @NgModule({
              declarations: [
                AppComponent,
                UserComponent,
                HeaderComponent,
                FooterComponent,
                MenuComponent,
                UserCardComponent,
                ItemComponent,
                ColoryDirective,
                DelayDirective,
                SomeItemComponent   <-----
              ],
              entryComponents: [ SomeItemComponent ],        <-----
              imports: [
                BrowserModule,
                AppRoutingModule
              ],

    в app.component.ts

        constructor(
          private viewContainerRef: ViewContainerRef,
          private componentFactoryResolver: ComponentFactoryResolver
        ) { }

    viewContainerRef - то куда мы будем добавлять наш динамический компонент
    componentFactoryResolver - то что позволяет собрать компонент


    теперь добавим на страницу компонент через некоторое время

    ngOnInit() {
        setTimeout( _ => {
          const componentFactory =  this.componentFactoryResolver.resolveComponentFactory( SomeItemComponent );
          const componentRef = this.viewContainerRef.createComponent(componentFactory);
        }, 8000 );
    }



    componentFactory - фабрика с нашим компонентом

    componentRef - ссылка на компонент

---

21. Сервисы - для работы с данными

            ng generate service user

            ng g s user


      Зарегистрируем сервис в главном модуле app.module.ts

        @NgModule({
          declarations: [
            AppComponent,
            UserComponent,
            HeaderComponent,
            FooterComponent,
            MenuComponent,
            UserCardComponent,
            ItemComponent,
            ColoryDirective,
            DelayDirective,
            SomeItemComponent
          ],
          entryComponents: [ SomeItemComponent ],
          imports: [
            BrowserModule,
            AppRoutingModule
          ],
          providers: [UserService],   <<<-----------------------
          bootstrap: [AppComponent]
        })
        export class AppModule { }



        Определим массив users и его геттер в сервисе

          import { Injectable } from '@angular/core';

          @Injectable({
            providedIn: 'root'
          })
          export class UserService {

            private users = [
              {name: 'Den'},
              {name: 'Max'},
              {name: 'John'}
            ] ;

            constructor() { }

            public getAll() {
              return this.users;
            }

          }


          теперь в компоненте получим массив users.

          для того чтобы подключить сервис к компоненту используетсч паттерн dependency injection


              app.component

              import { UserService } from './user.service';

              @Component({
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.sass']
              })
              export class AppComponent implements OnInit{
                public users;

                constructor(
                  private userService: UserService
                ) { }

                ngOnInit() {
                  this.users = this.userService.getAll();
                  console.log(this.users);
                }
              }

---

22. Сервисы. Внедрение зависимостей. CRUD

    в шаблоне

    <h2>Тут будет CRUD</h2>
    <ul>
      <li *ngFor="let user of users">
        {{user.name}} <span (click)="removeItem(user.name)"> x</span>
      </li>
    </ul>
    <br>
    <input type="text" #newUser><br>
    <button (click)="addUser(newUser.value)">Add user</button>

    в компоненте

    removeItem(name: string){
      this.userService.remove(name);
      this.users = this.userService.getAll();
    }

    addUser(name: string){
      if(!name){
        return ;
      }
      this.userService.add(name);
      this.users = this.userService.getAll();
    }

    в сервисе

    public getAll() {
      return this.users;
    }

    public remove(name: string) {
      return this.users = this.users.filter(user => user.name !== name);
    }

    public add(name: string) {
      this.users.push({ name } );
    }

---

23. HttpClient + ajax


      Подключим модуль в app.modules.ts


            import { HttpClientModule } from '@angular/common/http';   <--------

            @NgModule({
              declarations: [
                AppComponent,
                UserComponent,
              ],
              entryComponents: [ SomeItemComponent ],
              imports: [
                BrowserModule,
                AppRoutingModule,
                HttpClientModule          <--------
              ],
              providers: [UserService],
              bootstrap: [AppComponent]
            })
            export class AppModule { }


        в user.service

          public sendRequest(){
            return this.http.get('https://jsonplaceholder.typicode.com/users')
          }


        в app.components

        this.userService.sendRequest().subscribe(data => {
          this.users = data;
        });

---

24. Dependency Injection

    1. Provider
    2. Injector
    3. Dependency


      Provider  - определяется в модуле , определяется обьектом { token, recipe }

      в app.module

        providers: [
          // { token, recipe }
          { provide: UserService, useClass: UserService },   <<--- Идентичны
          UserService                                        <<--- Идентичны
        ]

---

25. Интерсепторы ( Middleware ) HTTPClient ---> Interceptrors ---> API

    ng g s myinterceptor


          import { Injectable } from '@angular/core';
          import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
          import { Observable } from 'rxjs';

          @Injectable({
            providedIn: 'root'
          })
          export class MyinterceptorService implements HttpInterceptor {

            constructor() { }

            intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
              const request  = req.clone({params : req.params.set( 'x', '5' )});

              return next.handle(request);
            }
          }


        в app.modules

        @NgModule({
        declarations: [
          AppComponent,
          UserComponent,
          HeaderComponent,
          FooterComponent,
          MenuComponent,
          UserCardComponent,
          ItemComponent,
          ColoryDirective,
          DelayDirective,
          SomeItemComponent
        ],
        entryComponents: [ SomeItemComponent ],
        imports: [
          BrowserModule,
          AppRoutingModule,
          HttpClientModule    <------------
        ],
        providers: [
          UserService,
          {                                                      <------------
            provide: HTTP_INTERCEPTORS,                          <------------
            useClass: MyinterceptorService,                      <------------
            multi: true                                          <------------
          }
        ],
        bootstrap: [AppComponent]
      })
      export class AppModule { }

---

26. Router


    app.modules


    const routes = [
      { path: '', component: HomeComponent },
      { path: 'about', component: AboutComponent }
    ];

    imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      RouterModule.forRoot(routes)    <----------------
    ],


    app.component


    <a routerLink="" routerLinkActive="my-active" [routerLinkActiveOptions]="{exact: true}">Home</a>
    <a routerLink="about" routerLinkActive="my-active">About</a>
    <a [routerLink]="['/contacts']" routerLinkActive="my-active">Contacts</a>
    <router-outlet></router-outlet>


    routerLinkActive  - задает класс активному элементу
    routerLinkActiveOptions - делаем так чтобы подсвечиваля только активный элемент, а не все кто подпадает под условие  / или /about

---

27. Работа с параметрами

    app.modules

    const routes = [
    { path: '', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'contacts', component: ContactsComponent },
    { path: 'users', component: UsersComponent },
    { path: 'users/:userId', component: UserComponent }
    ];


      user.component

          export class UserComponent implements OnInit {

            constructor(private route: ActivatedRoute) {
              this.route.params.subscribe(params => console.log(params))
            }

            ngOnInit() {
            }

          }



      <a routerLink="" routerLinkActive="my-active" [routerLinkActiveOptions]="{exact: true}">Home</a>
      <a routerLink="about" routerLinkActive="my-active">About</a>
      <a [routerLink]="['/contacts']" routerLinkActive="my-active">Contacts</a>
      <a [routerLink]="['/users']" routerLinkActive="my-active">Users</a>
      <a routerLink="users/12" routerLinkActive="my-active">User 12</a>    <-------------



      при http://localhost:4200/users/12    в консоли {userId: "12"}

      при  http://localhost:4200/users?q=15   в консоли {q: "15"} но для этого в users.component

                    constructor(private route: ActivatedRoute) {
                      this.route.params.subscribe(params => console.log(params))
                    }


     для того чтобы сразу вставить параметры в ссылку app.compomemt

     <a routerLink="users" [queryParams]="{q: 111}"  routerLinkActive="my-active">Users with query</a>

---

28, Как перенаправить пользователя на другой стейт иди другой роут

    допустим в компоненте home.html
    у нас есть кнопка и по клику на нее мы хотим перейти на другой стейт
    например на стейт какого-то пользователя

    <p>home works!
      <br>
      <button (click)="goToUser(17)">Кнопка</button>
    </p>

    Определим этот метод в home.component


        export class HomeComponent implements OnInit {
            constructor(private router: Router) { }   <------------------

            goToUser(userId){
              this.router.navigate(['users', userId]);      <------------------
              this.router.navigateByUrl('users/' + userId);   <------------------
              // skipLocationChange  - подгрузится нужный компонент, но урл не изменится
              this.router.navigateByUrl('users/' + userId, {skipLocationChange: true});
            }

          }

            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            this.router.navigateByUrl('/users/' + userId);  <-- путь от корня

            this.router.navigateByUrl('users/' + userId);   <-- добавит к существующему роуту

            Метод navigateByUrl возвращает промис

             this.router.navigateByUrl('users/' + userId).then( )

---

29, Вложенные стейты

    Допустим перешли на страничку пользователя конкретного с id 17

    А у него есть два таба  Profile  || Settings

    Внутри компонента User создадим два вложенных компонента profile и settings

      ng g c users/profile
      ng g c users/settings

В app.module добавим children в список routes

const routes = [
{ path: '', component: HomeComponent },
{ path: 'about', component: AboutComponent },
{ path: 'contacts', component: ContactsComponent },
{ path: 'users', component: UsersComponent },
{ path: 'users/:userId', component: UserComponent , children: [
    { path: 'profile', component: ProfileComponent },
    { path: 'usersettings', component: SettingsComponent }
]}
];

Теперь в шаюлоне user.component определим гду будудт выводитбся наши дочерние компоненты

  <p>user works!</p>
  <a routerLink="profile" routerLinkActive="router-link-active" >Profile</a>
  <a routerLink="settings" routerLinkActive="router-link-active">Settings</a>

<router-outlet></router-outlet>

и теперь внутри стейта мы подгружаем дочерние стейты

---

30. События в роутере


    user.component.ts   добавим route


       constructor(
          private route: ActivatedRoute,
          private router: Router) {  <-------------------------
            this.router.events.subscribe((event: Event) => {
              console.log(event) ;
              // выполнение действий если произошло событе navigation Start
              if(event instanceof NavigationStart){
                console.log('Navigation starttt') ;
              }
            })
        }}

Таким образом мы ослеживаем различные события на странице по переходу с использованием роутера

Можно использовать например навигатион старт для отслеживания перехода на страницу и навигатион энд ухода

---

31. Гарды ( ХУКИ )

    app.modules

    /\*
    С помощью хуков остеживаются события до перехода на страницу

    - CanActivate/CanActivateChild - может ли роутер активировать данный стейт
    - CanDeactivate
    - CanLoad
    - Resolve

    \*/

    Мы хотим запретить доступ к стейту для неавторизированного пользователя

    создадим гуард auth

    ng g guard auth


            @Injectable({
              providedIn: 'root'
            })
            export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
              canActivate(
                next: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
                return true;
              }
              canActivateChild(
                next: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
                return true;
              }
              canLoad(
                route: Route,
                segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
                return true;
              }
            }

    canActivate возвращает true  -все ок можно давать доступ


    т.к. наш authGuard сервис, то мы подключим его в провайд в app.module


            import { AuthGuard } from './auth.guard';

            providers: [
              UserService,
              AuthGuard   <---------------------
            ],

    теперь повесим этот гард на путь users

            const routes = [
              { path: '', component: HomeComponent },
              { path: 'about', component: AboutComponent },
              { path: 'contacts', component: ContactsComponent },
              { path: 'users',
                  canActivate: [ AuthGuard ],    <-----------------------
                  component: UsersComponent
              },
              { path: 'users/:userId', component: UserComponent , children: [
                { path: 'profile', component: ProfileComponent },
                { path: 'settings', component: SettingsComponent}
              ]}
            ];

     если мы поставим false то по этиой ссылке уже нельзя перецйти

             canActivate(
                next: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
                return false;
              }


    также можно делать асинхронную проверку

    создадим сервис ng g s auth  и у него метод isAuth()

     canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.authService.isAuth();
    }

---

33. Ленивая загрузка модулей

Когда не все стейты активно используются и нам не нужно сразу грузить стейт при заходу на страницу

    Итак например у нас есть модуль админки. И нам не нужно ее везде постоянно загружать

    Создадим модуль

      ng g m admin

      у нас создалась папки admin/admin.module

    В app.module

    const routes = [
        { path: '', component: HomeComponent },
        { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },  <------
        { path: 'about', component: AboutComponent }
    ]

    указали путь до модуля и через #название модуля


    создадим внутри модуля admin компонент admin

        ng g c admin/admin

    Теперь в самом модуле

    admin.module

          import { RouterModule, Routes } from '@angular/router';     <------
          import { AdminComponent } from './admin/admin.component';   <------

          const routes: Routes = [                              <------
              { path: '', component: AdminComponent}            <------
          ];                                                    <------

          @NgModule({
            declarations: [AdminComponent],                     <------
            imports: [
              CommonModule,
              RouterModule.forChild(routes)                                      <------
            ]
          })
          export class AdminModule { }


    app.component.html

            <a routerLink="admin"  routerLinkActive="my-active">Admin</a>


    Теперь при загрузке главной страницы не подгружается сразу наш admin
    он подгрузится только когда мы кликнем по ссылке admin

    ************************************************************************

    34. Стратегия предзагрузки модулей

        Пользователь кликает на ссылку и через пару секунд загружается подгружается стейт.  Это доворльно долго

        Как же сделать чтобы модуль загружался при загрузке приложения, но в последнюю очередб после всех остальных?


    app.module

         imports: [
            BrowserModule,
            AppRoutingModule,
            HttpClientModule,
            RouterModule.forRoot(routes , {preloadingStrategy: PreloadAllModules})     <--------
          ],


    Создадим файл в корне приложения  custom-preloading-strategy.ts


            import { PreloadingStrategy, Route } from '@angular/router';
            import { Observable, of} from 'rxjs';


            export class CustomPreloadingStrategy implements PreloadingStrategy {
              preload(route: Route, load: () => Observable<any>): Observable<any>{
                // return of(null);   // если не хотим делать предзагрузку
                //  return load();
                of(true).delay(5000).do( _=>{
                  load();
                })
              }
            }


      ЭТОТ ВАРИНАТ НЕРАБОЧИЙ ИБО DO УЖЕ НЕ РАБОТАЕТ НУЖНО ИСКАТЬ PIPE TAP


      Ну и в appюmodule указать CustomPreloadingStrategy

---

35. Реактивные формы

Подключим FormsModule и ReactiveFormsModule

В app.module

            import { FormsModule, ReactiveFormsModule } from '@angular/forms';


            imports: [
              FormsModule,
              ReactiveFormsModule
            ],

В реактивных формах все начинается с сущности FormControl

        ng g c reactive-forms

        export class ReactiveFormsComponent implements OnInit {

          nameControl: FormControl;

          ngOnInit() {
            this.nameControl = new FormControl('John');
            // отслеживаем изменение инпута
            this.nameControl.valueChanges.subscribe( (val) => { console.log(val)} );
            // отслеживаем изменение статуса  ( Валидна ли форма  VALID)
            this.nameControl.statusChanges.subscribe( (status) => { console.log(status)} );
          }

        }

    reactive-forms.component.html  к инпуцту привяжем свойство formControl

        <input type="text" [formControl]="nameControl">

Поставим валидацию для нашего поля

        import { FormControl , Validators} from '@angular/forms';

        this.nameControl = new FormControl('John', [Validators.required, Validators.minLength(5) ] );

Создадим свой валидатор

        this.nameControl = new FormControl('John', [ myValidator ] );

        function myValidator( formControl: FormControl){
          if( formControl.value.length < 3  ){
              return {  myValidator: { message: 'My value should be longer then 3'} };
          } else {
              return null;
          }
        }

Асинхронный валидатор - например проверка пользователя на существование в бд

      this.nameControl = new FormControl('John', [ myValidator ], [myAsyncValidator] );

      function myAsyncValidator( formControl: FormControl): Observable<null|any>{
        // http.get
        if( formControl.value.length < 3  ){
            return of({ myAsyncValidator: { message: 'My value should be longer then 3'} });
        } else {
            return of(null);
        }
      }

---

36. FormGroup и FormArray


    Если же у нас несколько инпутов и они представляют общий FieldSet то используем FormGroup


    reactive-form.component.html

          <fieldset [formGroup]='fullNameControl'>
            <input type="text" formControlName="firstName" >
            <input type="text" formControlName="lastName">
          </fieldset>

     reactive-form.component.ts


          export class ReactiveFormsComponent implements OnInit {

            fullNameControl: FormGroup;

            constructor() { }

            ngOnInit() {

              this.fullNameControl = new FormGroup({
                  firstName: new FormControl(),
                  lastName: new FormControl()
              });
              this.fullNameControl.valueChanges.subscribe( (val) => { console.log(val)} );
            }
          }

formArray - позволяет работать со списками

reactive-form.component.html

          <form [formGroup]="userListControl">
            <ul formArrayName="users">
              <li *ngFor="let userControl of userListControl.controls.users.controls;let i = index ">
                <input type="text" formControlName="{{i}}">
                <button (click)="removeUserControl(i)">Remove</button>
              </li>
            </ul>
          </form>
          <button (click)="addUserControl()">Add user</button>

reactive-form.component.ts

      export class ReactiveFormsComponent implements OnInit {

          userListControl: FormGroup;

          constructor() { }

          ngOnInit() {

            this.userListControl =  new FormGroup({
                  users:  new FormArray([
                                new FormControl('Alice'),
                                new FormControl('Den'),
                                new FormControl('Max')
                              ])
            });
            this.userListControl.valueChanges.subscribe( (val) => { console.log(val)} );
          }
          addUserControl(){
            (this.userListControl.controls['users'] as FormArray).push( new FormControl(''));
          }

          removeUserControl(index: number){
            (this.userListControl.controls['users'] as FormArray).removeAt(index);
          }

        }

В консоль логе увидим array(3)

---

37. FormBuilder для создания сложных форм

FormBuilder - сервис который можно заинжектировать

    Заинжектим в конструкторе

     constructor( private formDuilder: FormBuilder) { }

    И теперь заменим длинное описание на короткое

            // this.userListControl =  new FormGroup({
            //       users:  new FormArray([
            //                     new FormControl('Alice'),
            //                     new FormControl('Den'),
            //                     new FormControl('Max')
            //                   ])
            // });

            равноценно

             this.userListControl = this.formDuilder.group({
                users: this.formDuilder.array([ ['Alice'], ['Den'], ['Max'] ])
             })

---

38. Тестирование сервиса

    ng g s calculator

            export class CalculatorService {

              sum(a: number, b: number): number {
                return a + b;
              }

              sumAsync(a: number, b: number): Promise<number> {
                return new Promise( resolve => {
                  setTimeout( _ => {
                    resolve(a + b);
                  }, 3000);
                });
              }

            }

    calculator.service.spec.ts


            import { TestBed, fakeAsync, flush } from '@angular/core/testing';

            import { CalculatorService } from './calculator.service';

            describe('CalculatorService', () => {
              beforeEach(() => TestBed.configureTestingModule({}));

              it('should be created', () => {
                const service: CalculatorService = TestBed.get(CalculatorService);
                expect(service).toBeTruthy();
              });

              it('should return sum', () => {
                const service: CalculatorService = TestBed.get(CalculatorService);
                expect(service.sum(2, 3)).toBeTruthy(5);
              });

              it('should return async sum', fakeAsync(() => {
                const service: CalculatorService = TestBed.get(CalculatorService);
                service.sumAsync(2, 3).then(result => {
                  expect(result).toBeTruthy(5);
                });
                flush();
              }));

            });

---

39. RxJS

https://github.com/javascriptru/rxjs-typescript-starter.git
  
 npm i  
 npm start

    localhost:3000


    Реактивное программированиен RxJS

    Допустим у нас есть инпут в index.html

      <body>
      <input type="text">

    В index.ts напишем обраьотчик

    // обработка коллбэком
            const myInput = document.querySelector('input');
            myInput.addEventListener('input', (event: KeyboardEvent) => {
              console.log( (event.target as HTMLInputElement).value);
            })

     // обработка промисом
            const myInput = document.querySelector('input');

            function createEventPromise(){
              let listener : (event: KeyboardEvent) => void;
              const p = new Promise( (resolve) => {
                listener = (event) => {
                  resolve(event);
                }
                myInput.addEventListener('input', listener);
              });

              p.then( (event: KeyboardEvent) => {
                // смотрим кол-во репозиториев по конкретному запросу
                  fetch(`https://api.github.com/search/repositories?q=${(event.target as HTMLInputElement).value}`)
                  .then( response => response.json() )
                  .then( response => {
                    console.log(response.total_count);
                  } );
                console.log( (event.target as HTMLInputElement).value);
                myInput.removeEventListener('input', listener);
                createEventPromise();
              });
            }

            createEventPromise();

Но это все фигня) Нам нужно пересоздавать промис каждый раз при вводе каждого символа

    // обработка при помощи реактивного RxJS
          const myInput = document.querySelector('input');

          import { fromEvent } from  'rxjs';
          // switchMap - переключить наш поток
          import { switchMap, debounceTime } from 'rxjs/operators';

          // создаем обозреваемую сущность observable
          const observable = fromEvent(myInput, 'input');
          // debounceTime - задержка перед отправкой запроса на сервер 500ms
          observable.pipe( debounceTime(500), switchMap( (event: KeyboardEvent) => {
          return fetch(`https://api.github.com/search/repositories?q=${(event.target as HTMLInputElement).value}`)
          .then( response => response.json() )
          })).subscribe( response => console.log(response.total_count) );

---

40. Что такое SwitchMap


    Допустим есть у нас RxJs и кнопка


        <script src="https://unpkg.com/@reactivex/rxjs@5.3.0/dist/global/Rx.js"></script>

        <button>Click me</button>

    в js есть два потока

        var button = document.querySelector('button');
        // поток1
        var obs1 = Rx.Observable.fromEvent(button, 'click');
        // поток2
        var obs2 = Rx.Observable.interval(1000);

        // при клике на кнопку срабатывает поток1 и вешает поток 2 на событие
        // тоесть каждую секунду выводится 0 1 2 3 4 5
        obs1.subscribe(
            event => obs2.subscribe(
              val => console.log(val)
            )
        )

    но вот проблема в том что сли мы еще раз кликнем на кнопку, то у нас одновременно будут два потока идти

    0 1 2 3 4 5 6 0 7 1 8 2 9

    А это фигня.  Нужно при клике запускать новый поток

    Сделаем так

        obs1.switchMap(
            event => {
              return obs2
            }
        ).subscribe(
          value => console.log(value)
        )


    Теперь при клике на кнопку каждый раз запускаетсф счетчик с нуля

    0 1 2 3 4 0 1 2 3 4 5 0 1 2 3



    **************************************************************************

    41. Фабрика создания Observable


        import { Observable, Observer } from 'rxjs';

        // создаем новый обсервабл через метод create
        const o = Observable.create( (observer: Observer<string>)=>{
            // метод observer closed
            // метод observer complete - завершить поток
            // метод observer error  - передать ошибку
            // метод observer next   - передать следующее значение
            observer.next('Hello!');
            observer.next('Hello!');
            observer.next('Hello!');
            setInterval( _ => {
              observer.next('Hello!');
            }, 1000)
            setTimeout( _ =>{
              observer.complete();
            }, 10000)
        } );

        o.subscribe({
            next: (value : string) => console.log('next',value),
            complete: () => console.log('Complete!'),
            error: (error) => console.log('next', error)

        });

---

42. Другие методы создания observable

Методы класса HttpClient после выполнения запроса возвращают объект Observable<any>, который определен в библиотеке RxJS ("Reactive Extensions"). Она не является непосредственно частью Angular, однако широко используется особенно при взаимодействии с сервером по http. Эта библиотека реализует паттерн "асинхронный наблюдатель" (asynchronous observable).

    import { of } from 'rxjs';

    const o = of(5, 6, 7, 8, 9);   // В промисах было б Promise.resolve(5)

    o.subscribe({
        next: (value : any) => console.log('next',value),
        complete: () => console.log('Complete!'),
        error: (error) => console.log('next', error)
    });

> > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >

    timer

    import { timer } from 'rxjs';
    const o = timer(0, 500)   // 0 - старт  500- периодичность

     o.subscribe({
        next: (value : any) => console.log('next',value),
        complete: () => console.log('Complete!'),
        error: (error) => console.log('next', error)
    });

> > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >

    interval

    import { interval } from 'rxjs';
    const o = interval(500)   // 500- периодичность

     o.subscribe({
        next: (value : any) => console.log('next',value),
        complete: () => console.log('Complete!'),
        error: (error) => console.log('next', error)
    });

> > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >

    range - последовательность в диапазоне

    import { range } from 'rxjs';
    const o = range(0, 100)

     o.subscribe({
        next: (value : any) => console.log('next',value),
        complete: () => console.log('Complete!'),
        error: (error) => console.log('next', error)
    });

> > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >

    empty - сразу закомплитит

    import { empty } from 'rxjs';
    const o = empty();

     o.subscribe({
        next: (value : any) => console.log('next',value),
        complete: () => console.log('Complete!'),
        error: (error) => console.log('next', error)
    });

> > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >

    throwError - сразу закомплитит

    import { throwError } from 'rxjs';
    const o = throwError('My strange error);

     o.subscribe({
        next: (value : any) => console.log('next',value),
        complete: () => console.log('Complete!'),
        error: (error) => console.log('next', error)
    });


    **********************************************

    43. Дженерики

        так плохо писать
                  function getter( data: any ){
                    return data;
                  }

                  console.log( getter('WFM').lenght );   //3
                  console.log( getter(100).lenght );   // undefined


      А вот дженерик

        function genericGetter<T> (data : T): T {
          return data;
        }

---

44. Методы фильтрации RxJS


          import { range } from 'rxjs';
          // import { timer } from 'rxjs';
          import { filter } from 'rxjs/operators';
          import { first } from 'rxjs/operators';
          import { last } from 'rxjs/operators';
          import { single } from 'rxjs/operators';
          // import { debounce, debounceTime } from 'rxjs/operators';
          import { debounce } from 'rxjs/operators';
          import { throttle, throttleTime } from 'rxjs/operators';
          import { audit, auditTime } from 'rxjs/operators';
          import { skip, skipLast, skipUntil, skipWhile } from 'rxjs/operators';
          import { /*take,*/ takeLast, takeUntil, takeWhile } from 'rxjs/operators';
          import { from } from 'rxjs';

          // pipe - метод в котором собраны все операции над потоком нашим
          // выведем все числа из диапазонам больше 50
            const o = range(0, 100).pipe( filter( number => number > 50));
          // выведем все числа из диапазонам больше 50 только первое
            const o1 = range(0, 100).pipe( first( number => number > 50));
          // выведем все числа из диапазонам больше 50 только последнее
            const o2 = range(0, 100).pipe( last( number => number > 50));
          // выведем все числа 50 только если оно одно
            const o3 = range(0, 100).pipe( single( number => number  == 50));
          // задержка 10 сек
            const o4 = range(0, 100).pipe( debounceTime( 10000 ));
          // вывести только уникальные значения
            const o5 = from([1, 1, 1, 2, 2, 3, 3, 3, 3, 5]).pipe( distinctUntilChanged());

          const o6 =  timer(0, 200).pipe(throttleTime(1000));
          // мы хотим пропустить  10 элементов
          const o7= range(0, 20).pipe( skip(10) );
          // мы хотим взять  10 элементов
          const o8= range(0, 20).pipe( take(10) );


          o.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });
          o1.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });
          o2.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });
          o3.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });
          o4.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });
          o5.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });
          //  o6.subscribe({
          //     next: (value : any) => console.log('next',value),
          //     complete: () => console.log('Complete!'),
          //     error: (error) => console.log('next', error)
          // });
          o7.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });
          o8.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });

---

45. 6 операторов RxJS которые должны знать


        оператор Of - задает набор элементов

            import { of } from 'rxjs';

            const source = of(1, 2, 3, 4, 5);

            //output: 1,2,3,4,5   <---------------
            const subscribe = source.subscribe(val => console.log(val));



            const source = of({ name: 'Brian' }, [1, 2, 3], function hello() { return 'Hello';});

            //output: {name: 'Brian'}, [1,2,3], function hello() { return 'Hello' }
            const subscribe = source.subscribe(val => console.log(val));


        оператор timer - задает задержку и и нтервал исполнения   timer( задержка, интервал )

              const source = timer(5000, 3000);
              // пройдет задержка 5 сек и каждые три секунды будет выводиться счетчик 0, 1, 2, 3, 4
              const subscribe = source.subscribe(val => console.log(val));

        оператор fromEvent - отслеживает события

              const clicks$ =  fromEvent(document, 'click');
              clicks$.subscribe(event => console.log(event));

              res:   выведет mouseEvent


        1. Concat

            const getPostOne$ = Rx.Observable.timer(3000).mapTo({id: 1});
            const getPostTwo$ = Rx.Observable.timer(1000).mapTo({id: 2});

            Rx.Observable.concat(getPostOne$, getPostTwo$).subscribe(res => console.log(res));

            result:

            > obj1
            > obj2

        2. forkJoin

            const getPostOne$ = Rx.Observable.timer(1000).mapTo({id: 1});
            const getPostTwo$ = Rx.Observable.timer(2000).mapTo({id: 2});

            Rx.Observable.forkJoin(getPostOne$, getPostTwo$).subscribe(res => console.log(res))

            result:

            >  Array( obj1 , obj2 )

        3. mergeMap

              const post$ = Rx.Observable.of({id: 1});
              const getPostInfo$ = Rx.Observable.timer(3000).mapTo({title: "Post title"});

              const posts$ = post$.mergeMap(post => getPostInfo$).subscribe(res => console.log(res));

              result:

              >  Obj { title: "Post title" }

            – применяется, когда у вас есть Observable, элементы последовательности которого тоже Observable, а вам хочется объединить все в один поток

        4.  pairwise

            – возвращает не только текущее значение, но в месте с ним и предыдущее значение последовательности

                  Rx.Observable
                  .fromEvent(document, 'scroll')
                  .map(e => window.pageYOffset)
                  .pairwise()
                  .subscribe(pair => console.log(pair)); // pair[1] - pair[0]

                  res

                      [43, 61],
                      [45, 63],
                      [47, 65]

          5. switchMap  - переключить поток. Завершить один и начать другой

                  const clicks$ = fromEvent(document, 'click');
                  const innerObservable$ = interval(1000);

                  clicks$.pipe(switchMap(event => innerObservable$)).subscribe(val => console.log(val));


                  res

                  0 1 2 3 0 1 2 3

                  switchMap делает complete для предыдущего Observable, то есть в данном случае у нас всегда будет только один активный Observable для интервала:

                  а вот mergeMap нам бы на каждый клик порождал новую interval последовательность.

        6. combineLatest

        – получить последние значения из каждой последовательности при эммите одного из них:

          const intervalOne$ = Rx.Observable.interval(1000);
          const intervalTwo$ = Rx.Observable.interval(2000);

          Rx.Observable.combineLatest(
              intervalOne$,
              intervalTwo$
          ).subscribe(all => console.log(all));

---

46. Методы трансформации


    // вот так в консоли мы получим весь обьект  { name: 'Bob', age : 33}
          const o  =  of(
            {
              name: 'Bob',
              age:33
            }
          )

          o.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });

> > > > > оператор PLUCK - А вот используя оператор pluck мы можем запросить только указанное свойство

    //  в консоли получим Bob
          const o  =  of(
            {
              name: 'Bob',
              age:33
            }
          ).pipe( pluck('name') );

          o.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });

> > > > > оператор REDUCE -Посчитаем сумму всех элементов через редюс

    // в консоли 15

          const o = of(1,2,3,4,5).pipe( reduce( (acc, b) => acc + b ) )

          o.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });

> > > > > оператор SCAN - типо reduce но получить значения на каждом шаге - оператор Scan

    // 1  3  6  10  15

          const o = of(1,2,3,4,5).pipe( scan( (acc, b) => acc + b ) )

          o.subscribe({
              next: (value : any) => console.log('next',value),
              complete: () => console.log('Complete!'),
              error: (error) => console.log('next', error)
          });

> > > > > оператор MAP - пробежать по массиву и преобразовать его

      // 2 4 6 8 10
      const o = of(1,2,3,4,5).pipe( map( n => n*2 ) )

      o.subscribe({
          next: (value : any) => console.log('next',value),
          complete: () => console.log('Complete!'),
          error: (error) => console.log('next', error)
      });

> > > > > оператор MapTo - преобразовать элементы массива к какому-то значению

      // Hi! Hi! Hi! Hi! Hi!
      const o = of(1,2,3,4,5).pipe( mapTo('Hi!') )

      o.subscribe({
          next: (value : any) => console.log('next',value),
          complete: () => console.log('Complete!'),
          error: (error) => console.log('next', error)
      });

> > > > > оператор flatMap и switchMap -

      flatMap  - обьединяет потоки в один
      switchMap  - запускает каждый раз новый поток

      const clicks = fromEvent(document, 'click');

      // 0 1 2 click 3 0 4 1 5 2
      const o = clicks.pipe( flatMap( _ => interval(1000) ) )
      //  0 1 2 click 0 1 2 3
      const o = clicks.pipe( switchMap( _ => interval(1000) ) )

      o.subscribe({
          next: (value : any) => console.log('next',value),
          complete: () => console.log('Complete!'),
          error: (error) => console.log('next', error)
      });

---

47. Обработка ошибок


        // 0 1 2 3 'My err > 3'
        const o = interval(2000).pipe( mergeMap(val => {
            if( val > 3){
              return throwError('My err > 3');
            }
            return of(val)
          }),
          catchError( (error) => {
            console.log(error);
            return of(false)
          } )
        );

        o.subscribe({
            next: (value : any) => console.log('next',value),
            complete: () => console.log('Complete!'),
            error: (error) => console.log('next', error)
        });

Оператор RETRY - указываем сколько раз попробовать снова перед тем как выдать ошибку

     // 0 1 2 3 0 1 2 3 0 1 2 3 'My err > 3'
        const o = interval(2000).pipe( mergeMap(val => {
            if( val > 3){
              return throwError('My err > 3');
            }
            return of(val)
          }),
          retry(2)
          //retryWhen( errorObservable  => errorObservable.pipe(delay(3000)))
        );


        o.subscribe({
            next: (value : any) => console.log('next',value),
            complete: () => console.log('Complete!'),
            error: (error) => console.log('next', error)
        });

---

48. Операторы утилиты

    TAP -вклинить например отладочную информацию

          const o = of(1,2,3,4,5).pipe( tap( n => {
            console.log( Math.random() )
          }) )

    DELAY - вывести последовательность с задержкой

          const o = of(1,2,3,4,5).pipe( delay(3000) , tap( n => {
            console.log( Math.random() )
          }) )


MININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININMININ


MININ VLADILEN

1.  Передача данных из ребенка к родителю

    В родителе

    > app.component.html
    > <app-post-form (onAdd)="updatePosts($event)" ></app-post-form>
    > app.component.ts

        updatePosts(post: Post){
          this.posts.unshift(post)
        }

    В ребенке

    > post.component.html
    > <button class="btn" (click)="addPost();">Add post</button>
    > post.component.ts

    import { Component, OnInit, Output, EventEmitter } from '@angular/core';

    @Output() onAdd: EventEmitter<Post> = new EventEmitter<Post>();

    addPost(){
      if (this.title.trim() && this.text.trim()) {
            const post: Post = {
              title: this.title,
              text: this.text
            };

            this.onAdd.emit(post);

            this.title = this.text = '';
        }

    }

..............................................................................................

2. Доступ к html средствами angular

  <div #formContainer></div>

import { ViewChild, ElementRef } from '@angular/core';
@ViewChild('formContainer', {static: true}) myDivRef: ElementRef;

ngOnInit() {
this.myDivRef.nativeElement.style.border = "1px solid red";
}

..............................................................................................

3. Передача HTML внутрь компонента

в родителе
<app-post
*ngFor="let p of posts"
[post]="p" >

          <small *ngIf="p.text.length > 10; else short">Пост длинный</small>
          <ng-template #short>
            <small>Пост короткий</small>
          </ng-template>

      </app-post>

в ребенке

       <ng-content ></ng-content>

............................................................................................

4. @ContentChild

> app.component.html

      <app-post
        *ngFor="let p of posts"
        [post]="p"
      >
        <div #info>  <-----------------
            <small *ngIf="p.text.length > 10; else short">Пост длинный</small>
            <ng-template #short>
              <small>Пост короткий</small>
            </ng-template>
        </div>

      </app-post>

> post.component.ts

    import { Component, OnInit, Input, ContentChild, ElementRef } from '@angular/core';

    @ContentChild('info', {static: true})  infoRef: ElementRef;

    ngOnInit() {
        console.log('nndnnd', this.infoRef.nativeElement)

    }

............................................................................................

5.  Жизненный цикл компонентов

    ngOnChanges - при изменении параметров = возвращает текущее и предидущее значение
    ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges', changes)
    }

          currentValue - текущее значение
          firstChange - первое ли изменение
          previousValue - предидущее значение


    ngOnInit - инициализация компонента

    ngDoCheck - вызывается на каждое изменение

    ngAfterContentInit() - вызывается когда инициализируется контент который мы передаем в компонент

          все внутри app-post

          <app-post>
              <div #info>
                  <small *ngIf="p.text.length > 10; else short">Пост длинный</small>
                  <ng-template #short>
                    <small>Пост короткий</small>
                  </ng-template>
              </div>
          </app-post>

     ngAfterContentChecked() - вызывается когда весь контент который мы передаем готов для использхования

     ngAfterViewInit() - вызывается когда вьюв инициализирована

            <div class="card">
              <h2>{{post.title}}</h2>
              <p>{{post.text}}</p>
              <ng-content ></ng-content>
            </div>

    ngAfterViewChecked() - вызывается когда вьюв готов

    ngOnDestroy - вызывается когда компонент будет уничтожен
                  Отписка от observable, EventHandler


                  Для прроверки удалим компонент

                  > post.component.html

                      <div class="card">
                        <h2>{{post.title}}</h2>
                        <p>{{post.text}}</p>
                        <button class="btn" (click)="removePost()">&times;</button>  <---
                      </div>

                   > post.component.ts

                    import {  EventEmitter } from '@angular/core';

                    @Output() onRemove = new EventEmitter<number>();

                    removePost(){
                        this.onRemove.emit(this.post.id)
                    }

                    > app.component.html

                      <app-post
                          *ngFor="let p of posts"
                          [post]="p"
                          (onRemove)="removePost($event)"  <-----
                        >
                          <div #info>
                              ...
                          </div>

                      </app-post>

                    > app.component.ts

                        removePost(id: number){
                          console.log(id)
                          this.posts = this.posts.filter( p => p.id !== id)
                        }

............................................................................................

6.  Инкапсуляция стилей

    По умолчанию стили все инкапсулированы, т.е действуют в пределах своего куомпонента.

    Однако мы можем отключить эту инкапсуляцию

    > post.component.ts

        import { ViewEncapsulation } from '@angular/core';

        @Component({
          selector: 'app-post',
          templateUrl: './post.component.html',
          styleUrls: ['./post.component.sass'],
          encapsulation: ViewEncapsulation.None    <----------   Стили станут глобальными для всего проекта
        })

............................................................................................

7. Директивы DIRECTIVES

   создадим директиву style.directive.ts


           import { Directive, ElementRef } from '@angular/core';

            @Directive({
              selector: '[appStyle]'
            })

            export class StyleDitrective{

                // el - это ссылка на элемент к которому мы прицепили директиву
               constructor(private el: ElementRef) {
                  console.log(el)
                  el.nativeElement.style.color = 'blue';
               }

            }

      зарегаем ее в модуле app.module


            import { StyleDitrective } from './directives/style.directive';  <--------
            import { FormsModule } from '@angular/forms';

            @NgModule({
              declarations: [
                AppComponent,
                StyleDitrective     <--------
              ],
              imports: [
                FormsModule
              ],
              providers: [],
              bootstrap: [AppComponent]
            })
            export class AppModule { }


      Навесим директиву на верстку

            <p appStyle>Lorem ipsum dolor sit amet.</p>

генерация директивы через CLI

            ng g d directives/style2 --skipTests

RENDERER2

The Renderer2 class is an abstraction provided by Angular in the form of a service that allows to manipulate elements of your app without having to touch the DOM directly. This is the recommended approach because it then makes it easier to develop apps that can be rendered in environments that don’t have DOM access, like on the server, in a web worker or on native mobile.

Basic Usage

          import { Directive, Renderer2, ElementRef, OnInit } from '@angular/core';

          @Directive({
            selector: '[appGoWild]'
          })
          export class GoWildDirective implements OnInit {
            constructor(private renderer: Renderer2, private el: ElementRef) {}

            ngOnInit() {
              this.renderer.addClass(this.el.nativeElement, 'wild');
            }
          }

createElement / appendChild / createText

          constructor(private renderer: Renderer2, private el: ElementRef) {}

          ngOnInit() {
            const div = this.renderer.createElement('div');
            const text = this.renderer.createText('Hello world!');

            this.renderer.appendChild(div, text);
            this.renderer.appendChild(this.el.nativeElement, div);
          }

setAttribute / removeAttribute

          constructor(private renderer: Renderer2, private el: ElementRef) {}

          ngOnInit() {
            this.renderer.setAttribute(this.el.nativeElement, 'aria-hidden', 'true');
          }

addClass / removeClass

          constructor(private renderer: Renderer2, private el: ElementRef) {}

          ngOnInit() {
            this.renderer.removeClass(this.el.nativeElement, 'wild');
          }

setStyle / removeStyle

          constructor(private renderer: Renderer2, private el: ElementRef) {}

          ngOnInit() {
            this.renderer.setStyle(
              this.el.nativeElement,
              'border-left',
              '2px dashed olive'
            );
          }



          constructor(private renderer: Renderer2, private el: ElementRef) {}

          ngOnInit() {
            this.renderer.removeStyle(this.el.nativeElement, 'border-left');
          }

set the value of an input field

        ngOnInit() {
          this.renderer.setProperty(this.el.nativeElement, 'value', 'Cute alligator');
        }

............................................................................................

8. События в Директивах

   import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

   @HostListener('click', ['$event.target']) onClick(event: Event){
   console.log(event);
   }


      При наведении на текст с директивой сделаем его красным а при уведении зеленым

      @HostListener('mouseover', ['$event.target']) onMouseover(event: Event){
        this.renderer.setStyle(this.el.nativeElement, 'color', 'red')
      }
      @HostListener('mouseleave', ['$event.target']) onCMouseleave(event: Event){
        this.renderer.setStyle(this.el.nativeElement, 'color', 'green')
      }

............................................................................................

9. Передача параметров внутрь директивы

Передадим цвет внутрь элемента с директивой appStyle >app.component.html

          <p appStyle="red">Lorem ipsum dolor sit amet.</p>

      >style.directive.ts

          // appStyle - указали что вытянуть с этой директывы значение color
          @Input('appStyle') color: string = 'blue';

          @HostListener('mouseover', ['$event.target']) onMouseover(event: Event){
            this.renderer.setStyle(this.el.nativeElement, 'color', this.color)
          }

Передадим параметр fontWeight в элемент c директивой appStyle

! в скобках [ ] пишем только если нужно число [top]='10', а если строка то и без скобок fontWeight="bold"

   <p appStyle="gold" fontWeight="bold" BigFontDirective>Lorem ipsum dolor sit amet.</p>

    @Input('appStyle') color: string = 'blue';
    @Input() fontWeight: string = 'normal';   <---------------

    @HostListener('mouseover', ['$event.target']) onMouseover(event: Event){
      this.renderer.setStyle(this.el.nativeElement, 'color', this.color)
      this.renderer.setStyle(this.el.nativeElement, 'fontWeight', this.fontWeight) <---------------
    }
    @HostListener('mouseleave', ['$event.target']) onCMouseleave(event: Event){
      this.renderer.setStyle(this.el.nativeElement, 'color', 'green')
      this.renderer.setStyle(this.el.nativeElement, 'fontWeight', 'normal') <---------------
    }

Также можем передать обьект со стилями

  <p appStyle="gold"   [dStyles]="{ border: '1px solid silver', borderRadius: '15px' }" >Lorem ipsum dolor sit amet.</p>

    @Input('appStyle') color: string = 'blue';
    @Input() dStyles: { border?: string, borderRadius?: string }


    @HostListener('mouseover', ['$event.target']) onMouseover(event: Event){
      this.renderer.setStyle(this.el.nativeElement, 'color', this.color)
      this.renderer.setStyle(this.el.nativeElement, 'border', this.dStyles.border)
      this.renderer.setStyle(this.el.nativeElement, 'borderRadius', this.dStyles.borderRadius)
    }
    @HostListener('mouseleave', ['$event.target']) onCMouseleave(event: Event){
      this.renderer.setStyle(this.el.nativeElement, 'color', 'green')
      this.renderer.setStyle(this.el.nativeElement, 'border', 'none')
      this.renderer.setStyle(this.el.nativeElement, 'borderRadius', '0')
    }

............................................................................................

10. @HostBinding

    При клике про элементу с нашей директивой appStyle сделать фон синим и дать класс myBorder


        @HostBinding('style.background') elBack = 'null';
        @HostBinding('class.myBorder') myBord: boolean = false;

        @HostListener('click', ['$event.target']) onClick(event: Event){
          this.elBack = 'lightblue';
          this.myBord = true;
        }

............................................................................................

11. Структурные директивы - меняют структуру html кода

*ngIf *ngFor \*ngSwitch

<div class="wrap" *ngIf="isVisible">
  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic rem a quasi! Ex, aspernatur quibusdam.</p>
</div>

Аналогично будет

<ng-template [ngIf]="isVisible">

  <div class="wrap" >
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic rem a quasi! Ex, aspernatur quibusdam.</p>
  </div>
</ng-template>

А теперь бахнем свою директиву структурную. Противоположную ngIf

ng g d directives/ifnot

> ifnot.directive

      import { Directive, ViewContainerRef, TemplateRef, Input } from "@angular/core";

      @Directive({
        selector: "[appIfnot]"
      })
      export class IfnotDirective {
        // сеттер
        @Input("appIfnot") set ifNot(condition: boolean) {
          if (!condition) {
            // показать элементы
            this.viewContainer.createEmbeddedView(this.templateRef);
          } else {
            //скрыть
            this.viewContainer.clear();
          }
        }

        //templateRef - содержимое ng-template
        //viewContainer - указывает ng-template

        constructor(
          private templateRef: TemplateRef<any>,
          private viewContainer: ViewContainerRef
        ) {}
      }


      >app.module

      @NgModule({
        declarations: [
          AppComponent,
          StyleDitrective,
          TestDirectiveDirective,
          BigFontDirective,
          IfnotDirective
        ],
        imports: [BrowserModule, FormsModule],
        providers: [],
        bootstrap: [AppComponent]
      })
      export class AppModule {}

      >app.component

        <div class="wrap" *appIfnot="isVisible">
          <p>My structure directive.</p>
        </div>


        <button  (click)="isVisible = !isVisible">show/hide</button>

---

12. PIPES

Number

    {{ e | number }}     // 2.718281828459045  ---> 2.718

    {{ e | number:'1.1-5' }}   // одно число до точки и пять после точки 2.17235

    {{ e | number:'3.1-2' }}   //  002.72

     {{ e | number:'1.0-0' }}  //  2

STRING

    {{ str | uppercase }}    // ANGULAR PIPES

    {{ 'Angular pipes' | titlecase }}    // Angular Pipes

    {{ 'Angular pipes' | slice:1 }}    //   ngular pipes

    {{ 'Angular pipes' | slice:1:3 }}    //   ng  Взяли строку с 1 по 3й элемент

    {{ 'Angular pipes' | slice:1:-2 }}  //   ngular pip    Второе идет с конца

JSON

    Извечная проблемма посмотреть что выведет на вью обьект


    obj:object =  {i:1, name: 'Den'}

    {{ obj  }}           //    [object Object]

    {{ obj | json }}     // { "i": 1, "name": "Den" }

Date

    {{ date | date: 'dd MMM yyyy' }}

Создаем собственный пайп РУКАМИ

> app.module

    @NgModule({
      declarations: [
        AppComponent,
        MultByPipe    <-------
      ],
      imports: [
        BrowserModule,
        FormsModule

      ],
      providers: [],
      bootstrap: [AppComponent]
    })

> pipes/mult-by.pipe.ts

    import { Pipe, PipeTransform } from "@angular/core";

    @Pipe({
      name: "multic"
    })
    export class MultByPipe implements PipeTransform {
      transform(num: number, pow: number = 2): number{
        return  num * pow;
      }
    }


    >app.component.html

    {{ 10 | multic}}    // 20
    {{ 10 | multic:3 }}    // 30

Создаем пайп через ANGULAR CLI

    ng g p pipes/add-pointers

    Хочу сделать пайп который обрезает строку и добавляет ... если длина строки больше 7

        import { Pipe, PipeTransform } from "@angular/core";

        @Pipe({
          name: "addPointers"
        })
        export class AddPointersPipe implements PipeTransform {
          transform(str: string): string {
            if (str.length > 7) {
              return `${str.slice(0, 7)}...`;
            } else {
              return str;
            }
          }
        }

      Результат
         {{ 'Loremasdasdasd ipsum dolor sit amet consectetur adipisicing.' | addPointers }}    // Loremas...

Пайп для фильтрации списков !!!

  ['''''''''''''''']

  Beer - Самое лучшее пиво в мире!

  Bread - The best bread in the world

  Water - The best water in the world


  ng g p pipes/filter --skipTests

  >filter.pipe.ts

    import { Pipe, PipeTransform } from "@angular/core";
    import { Post } from "../app.component";

    @Pipe({
      name: "filter"
    })
    export class FilterPipe implements PipeTransform {
      transform(posts: Post[], search: string = ''): Post[] {
        if ( !search.trim() ){
          return posts;
        } else {
          return posts.filter( post => {
            return post.title.toLowerCase().includes(search.toLowerCase());
          });
        }
      }
    }


  >app.component.ts

      import { Component } from '@angular/core';

      export interface Post {
        title: string;
        text: string;
      }

      @Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.scss']
      })
      export class AppComponent {

        search: string | number = '' ;

        posts: Post[] = [
          {title: 'Beer', text: 'Самое лучшее пиво в мире!'},
          {title: 'Bread', text: 'The best bread in the world'},
          {title: 'Water', text: 'The best water in the world'}
        ];
      }

  > app.component.html

    <input type="text" [(ngModel)]="search" />

    <div class="card" *ngFor="let post of posts | filter:search ">
      <p>{{ post.title }} - {{ post.text }}</p>
    </div>


*****************************************************************************

13. Асинхронный пайп

    export class AppComponent {
      p: Promise<string> = new Promise<string>(resolve => {
        setTimeout(() => {
          resolve("Promise resolved");
        }, 4000);
      });
    }

    {{ p | async }}   // wait for it Promise resolved


    ////////////////////////////////////////////////////////
    
    date: Observable<Date> = new Observable( obs => {
        setInterval(() => {
          obs.next( new Date());
        }, 1000);
    })


    Date {{ date | async }} 


*******************************************************************************

14. Сервис  - класс позволяет работать с данными

    @Injectable({
      providedIn: 'root'   // говорит о том что мы можем не рагеать его в апп.модуле
    })


    Если сервис зареган в шлавном модуле то он действует везде, но вот если м его добавим локально в компонент провайдерс, то он будет исключительно для жтого компонента , со своей отдельной областью видимости.

  *********************************************************************************

  15. Реактивные формы 

      Подключаем реактивные формы в app.module

          import { FormsModule, ReactiveFormsModule } from '@angular/forms';

          imports: [
            BrowserModule,
            FormsModule,
            ReactiveFormsModule
          ],

      >app.component.ts

          export class AppComponent implements OnInit {
            form: FormGroup;

            ngOnInit(){
              this.form = new FormGroup({})
            }
          }

      >app.component.html

          <form class="card" [formGroup]="form">  // связали форму в верстке с ts переменной forms
      
      Всем второстепенным кнопкам внутри формы нужно проставить типы type="button" ,а вот кнопке которая будет отправлять форму необходимо проставить type="submit"

          <button class="btn" type="button" >Выбрать столицу</button>
          <button class="btn" type="button">Добавить умение</button>

          <button class="btn" type="submit">Отправить</button>

      Теперь вешаем событие отправки формы и вызываем метод submit()

          <form class="card" [formGroup]="form" (ngSubmit)="submit()">

      И соответственнов в app/component добавим метод submit(){ }



      FormControl

          подключим к контролю инпуты email и password

          >app.module.ts

              ngOnInit(){
                  this.form = new FormGroup({
                    email: new FormControl(''),      <------- передаем начальное значение '' и через запятую список валидаторов
                    password: new FormControl(null), <------- передаем начае значение null и через запятую список валидаторов
                  })
                }

          теперь нажав на submit в консоли мы в ответе получим в обьекте свойство controls внутри которого будут
          email и password

          теперьв верстке свяжем инпуты с контролами через formControlName

                <input type="text" placeholder="Email" formControlName="email">
                <input type="password" placeholder="Пароль" formControlName="password">


          С помощью спреад оператора можем вывести значения формы в отдельный обьект

                submit(){
                    const formData = {...this.form.value};
                    console.log('formData', formData);
                }

                получим

                {
                  "email": "asd@asd.asd",
                  "password": "fghfgh"
                }

        Валидаторы

            >app.component.ts

               ngOnInit(){
                  this.form = new FormGroup({
                    email: new FormControl('', [ Validators.email, Validators.required ]  ),
                    password: new FormControl(null, [ Validators.minLength(7), Validators.required ]),
                  })
                }

            Но форма как отправлялась так и отправляется , меняется лишь значение forms.invalid= true

            Значит на кнопку отправки формы повесим аттрибут disabled

                <button class="btn" type="submit" [disabled]="form.invalid">Отправить</button>

            Все готово !!!!! 

            Если форма невалидна то и кнопка не нажимается


            ng-untouched  - не трогали             - ng-touched
             ng-pristine  - ничего не вписывали    - ng-dirty
              ng-invalid  - не валидный            - ng-valid


              input{
                &.ng-invalid.ng-touched{
                  border: 1px solid red;
                }
                &.ng-valid{
                  border: 1px solid green;
                }
              }


Ручной вывод сообщений об ошибках в форме

        <input type="text" placeholder="Email" formControlName="email">
        <div
          *ngIf ="form.get('email').invalid && form.get('email').touched"
          class="validation">
          <small *ngIf ="form.get('email').errors['required']">Поле email не может быть пустым</small>
          <small *ngIf ="form.get('email').errors['email']">Введите корректный Email</small>
        </div>

        <input type="password" placeholder="Пароль" formControlName="password">
        <div
          *ngIf ="form.get('password').invalid && form.get('password').touched"
          class="validation">
            <small *ngIf ="form.get('password').errors['required']">Поле пароль не может быть пустым</small>
            <small *ngIf ="form.get('password').errors['minlength']">
              Минимальная длина пароля {{ form.get('password').errors['minlength'].requiredLength }} символов
            </small>
        </div>


      Добавили класс validation и теги small

      Также добавили *ngIf   к значениям мы можем дотянуться через метод get

      form.get('email').invalid = true

Создание групп

    По аналогии с  FormControl

    ngOnInit(){
        this.form = new FormGroup({
          email: new FormControl('', [ Validators.email, Validators.required ]  ),
          password: new FormControl(null, [ Validators.minLength(7), Validators.required ]),
          address:  new FormGroup({                         <----------------
            country: new FormControl('ru'),                 <----------------
            city: new FormControl('', Validators.required)  <----------------
          })
        })
      }


      <div class="card" formGroupName="address">
         <select formControlName="country">
            <option value="ru">Россия</option>
            <option value="ua">Украина</option>
            <option value="by">Беларусь</option>
          </select>

           <input type="text"  formControlName="city" placeholder="Город...">
      </div>


      при отправке формы получим

              address: Object { country: "ua", city: "Днепр" }
              ​
              email: "asd@asd.sda"
              ​
              password: "1234567"

      Тоесть сделали декомпозицию,  вынесли в отдельный обьект address


Динамическое обновление формы !!!
Теперь сделаем чтобы при выборе в селекте страны в соседний инпут подставлялся город

   <button class="btn" type="button" (click)="setCapital()">Выбрать столицу</button>

    setCapital(){
      const cityMap = {
        ru: 'Москва',
        ua: 'Киев',
        by: 'Минск'
      }

      const cityKey = this.form.get('address').get('country').value;
      const city = cityMap[cityKey];

      this.form.patchValue({ address: { city: city } })
    }


Внутри формы добавляем новые инпуты динамически.

  Причем эти инпуты нужно валидировать


    // обязательно укажем что это ФормГруппа  formGroupName="skills"
    <div class="card" formGroupName="skills">   <-------
        <h2>Ваши навыки</h2>

        // добавили (click)="addSkill()"
        <button class="btn" type="button" (click)="addSkill()">Добавить умение</button>

        <div
          class="form-control"
          // добавили *ngFor
          *ngFor="let control of form.get('skills').controls; let idx = index "
        >
         <label> Умение {{idx+1}}</label>
         привязали форм контрол для работы валидации  В [] тк строка
         <input type="text" [formControlName]="idx">  <---------
        </div>
      </div>


      в компоненете добавили skills

          ngOnInit(){
            this.form = new FormGroup({
              email: new FormControl('', [ Validators.email, Validators.required ]  ),
              password: new FormControl(null, [ Validators.minLength(7), Validators.required ]),
              address:  new FormGroup({
                country: new FormControl('ua'),
                city: new FormControl('', Validators.required)
              }),
              skills: new FormArray([])     <----------
            })
          }

          addSkill(){
            const control = new FormControl('', Validators.required);
            // <FormArray>this.form.get('skills').push()
            (this.form.get('skills') as FormArray).push( control );
          }


  На выходе получили красивый обьект

    formData 
        {…}
        ​
        address: Object { country: "ua", city: "Киев" }
        ​
        email: "denis@gmail.com"
        ​
        password: "1234567"
        ​
        skills: Array [ "фыв", "фыв222" ]


Свой валидатор

В корневой папке создадим файл my.validators.ts

        import {FormControl} from '@angular/forms'

        export class MyValidators {

          // запрещенные имейлы
          static restrictedEnails( control: FormControl) : {[key: string]: boolean } {
              if( ['v@mail.ru', 'test@mail.ru'].includes(control.value) ){
                return {
                  'restrictedEmail': true
                }
              }
              return null
          }
        }

    >app.component.ts

      import {MyValidators} from './my.validators';

      ngOnInit(){
        this.form = new FormGroup({
          email: new FormControl('', [ 
            Validators.email,
             Validators.required,
              MyValidators.restrictedEnails   <------------
          ]  ),
          password: new FormControl(null, [ Validators.minLength(7), Validators.required ]),
          address:  new FormGroup({
            country: new FormControl('ua'),
            city: new FormControl('', Validators.required)
          }),
          skills: new FormArray([])
        })
      }

    >app.component.html

         <div
          *ngIf ="form.get('email').invalid && form.get('email').touched"
          class="validation">

          <small *ngIf ="form.get('email').errors['restrictedEmail']">Данный email запрещен</small>
        </div>


Асинхронные валидаторы  - проверка данных на сервере. Занят ли уже данный имейл

      import { FormControl } from "@angular/forms";
      import { Observable } from "rxjs";

      export class MyValidators {

        static uniqEmail(control: FormControl): Promise<any> | Observable<any> {
          return new Promise(resolve => {
            setTimeout(_ => {
              if (control.value === "async@mail.ru") {
                resolve({
                  uniqEmail: true
                });
              } else {
                return resolve(null);
              }
            }, 3000);
          });
        }
      }


       >app.component.ts

        import {MyValidators} from './my.validators';

          ngOnInit(){
            this.form = new FormGroup({
              email: new FormControl('', [ 
                Validators.email,
                Validators.required,
                
              ] ,
                MyValidators.restrictedEnails   <-----  асинхронный валидатор передается третьям параметровм
              ),
              password: new FormControl(null, [ Validators.minLength(7), Validators.required ]),
              address:  new FormGroup({
                country: new FormControl('ua'),
                city: new FormControl('', Validators.required)
              }),
              skills: new FormArray([])
            })
          }

        >app.component.html

         <div
          *ngIf ="form.get('email').invalid && form.get('email').touched"
          class="validation">
        
          <small *ngIf ="form.get('email').errors['uniqEmail']">{{form.get('email').value}} уже существует</small>
        </div>


Очистка формы

     submit(){
        const formData = {...this.form.value};

        this.form.reset();  <---------
      }




Angular HTTP


//////////////////////////////////////////////////////////////////////////////////

GET для чтения данных 

    app.module

        import { HttpClientModule } from '@angular/common/http';

        @NgModule({
          declarations: [
            AppComponent
          ],
          imports: [
            BrowserModule,
            HttpClientModule    <-----
          ],
          providers: [],
          bootstrap: [AppComponent]
        })

    app,component.ts

        import { HttpClient } from '@angular/common/http';

        export interface Todo {
          completed: boolean;
          title: string;
          id?: number;
        }

        export class AppComponent {

          todos: Todo[] = []

          constructor( private http: HttpClient ){
          }

          ngOnInit(){
            // тут дженерикомм укажем что ожидаем массив Todo[]
              this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?_limit=3').subscribe( res => {
                this.todos = res
              })
          }

        }


    app.component.html

         <div class="card" *ngFor="let todo of todos; let i = index">
          <p>
            <span [class.completed]="todo.completed">{{todo.title}}</span>
            <span>
              <button class="btn btn-danger">Удалить</button>
              <button class="btn" [disabled]="todo.completed">Завершить</button>
            </span>
          </p>
        </div>
        
/////////////////////////////////////////////////////////////////////////////////////////////////
POST - для создания новых элементов

 
!!!!!!  ДЛЯ ТОГО ЧТОБЫ МОЖНО БЫЛО ФОРМЕ ЗАДАТЬ [(ngModel)]="todoTitle"  НЕОБХОРДИМО ОБЯЗАТЕЛЬНО В МОДУЛЕ ИМПОРТИРОВАТЬ FormsModule

  import { FormsModule } from '@angular/forms';

  imports: [
    FormsModule
  ],



  Сделаем запись новой тудухи

   <div class="card">
      <div class="form-control">
        <input type="text" placeholder="Название" [(ngModel)]="todoTitle" >
      </div>
      <button class="btn" (click)="addTodo()">Добавить</button>
      <button class="btn">Загрузить</button>
    </div>


    app.component,ts


    todoTitle = ''


    addTodo(){
        // если строка состоит только из пробелов
        if( !this.todoTitle.trim() ){
          return
        }

        const newTodo: Todo = {
          title: this.todoTitle,
          completed: false
        }

        // длженериком явно укажем тип данных с которым работаем 
        this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', newTodo).subscribe( res => {
        console.log('post', res)
        this.todos.push(res)
      })

    }

//////////////////////////////////////////////////////////////////////////////////////

Операторы RxJS и ИНДИКАТОР ЗАГРУЗКИ

    по нажатию на кнопку загрузить выведем список всех todos

    app.module.ts

        import {delay} from 'rxjs/operators'

        loading = false

        fetchTodos(){
            this.loading = true;
            this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?_limit=3').pipe(delay(3000)).subscribe( res => {
              this.todos = res
              this.loading = false
            })
        }

     app.module.html

          <div  *ngIf='!loading; else loaderBlock'>
            <div class="card" #cardBlock>
              <div class="form-control">
                <input type="text" placeholder="Название" [(ngModel)]="todoTitle" >
              </div>
              <button class="btn" (click)="addTodo()">Добавить</button>
              <button class="btn" (click)="fetchTodos()">Загрузить</button>
            </div>
              ...
          </div>
          <ng-template #loaderBlock>Loading...</ng-template>

//////////////////////////////////////////////////////////////////////////////////////

DELETE

    удалить элемент todo

      <button class="btn btn-danger" (click)="removeTodo(todo.id)">Удалить</button>

      removeTodo( id: number){
        this.http.delete<void>( `https://jsonplaceholder.typicode.com/todos/${id}` ).subscribe( ()=>{
          this.todos = this.todos.filter( t =>  t.id != id )
        })
      }


/////////////////////////////////////////////////////////////////////////////////

Перенос логики в сервис

 <button class="btn btn-danger" (click)="removeTodo(todo.id)">Удалить</button>
<button class="btn" [disabled]="todo.completed" (click)="completeTodo(todo.id)">Завершить</button>


    ng g s todos



        import { Injectable } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { Observable } from 'rxjs';
        import {  map } from 'rxjs/operators';
        import {delay} from 'rxjs/operators';


        // Интерфейс лучше держать в сервисе
        export interface Todo {
          completed: boolean;
          title: string;
          id?: number;
        }


        @Injectable({
          // регистрация в корневом модуле
          providedIn: 'root'
        })
        export class TodosService {

          constructor( private http: HttpClient ) { }


          addTodo( todo: Todo ): Observable<Todo>{
            // Инициализируем стрим
            return this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', todo)

          }

          fetchTodos(): Observable<Todo[]>{
            return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?_limit=3').pipe(delay(1500))
          }

          removeTodo(id: number): Observable<void>{
            return  this.http.delete<void>( `https://jsonplaceholder.typicode.com/todos/${id}` )
          }

          completeTodo(id: number): Observable<Todo>{
            return  this.http.put<Todo>(`https://jsonplaceholder.typicode.com/todos/${id}`, { completed: true} )
          }


        }


///////////////////////////////////////////////////////////////////

Обработка ошибок

У метода subscribe есть два коллбэка   

    .subscribe( res => {
        ...
    }, error => { console.log(error.message);
    } )



    error: string = '';

    fetchTodos(){
      this.loading = true;
      this.todosService.fetchTodos().subscribe( res => {
        this.todos = res
        this.loading = false
      }, error => {                      <-------
        // console.log(error);
        this.error = error.message;
      } )
    }


    или же в сервисе через пайп


    import { Observable, throwError } from 'rxjs';
    import {  map, catchError } from 'rxjs/operators';

      fetchTodos(): Observable<Todo[]>{
        return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?_limit=3')
        .pipe(delay(1500))
        .pipe( catchError( error => {                 <-------
          console.log('ERROR', error.message )
          return  throwError(error)
          })
        )
      }


///////////////////////////////////////////////////////////////////

Заголовки

Запрос OPTIONS проверяет доступен ли вообще сервер

  import { HttpClient, HttpHeaders } from '@angular/common/http';

  addTodo( todo: Todo ): Observable<Todo>{
    // Инициализируем стрим
    const headers = new HttpHeaders().append('MyOwnHeader', 'Denis cool')
    return this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', todo , {headers: headers})
  }



///////////////////////////////////////////////////////////////////


Работа с параметрами

    import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


    fetchTodos(): Observable<Todo[]>{

        let params = new HttpParams().set('_limit', '5').append('custom', 'anything')

        return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos', {params: params })
        .pipe(delay(1500))
        .pipe( catchError( error => {
          console.log('ERROR', error.message )
          return  throwError(error)
          })
        )
      }


    https://jsonplaceholder.typicode.com/todos?_limit=5&custom=anything


  //////////////////////////////////////////////////////////////////////////////

  Параметр Observe

        после params укажем observe

   

BODY
    fetchTodos(): Observable<Todo[]>{
      let params = new HttpParams().set('_limit', '5').append('custom', 'anything')
        return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos', {params: params , observe: 'body' })
        .pipe(delay(1500))
        .pipe( catchError( error => {
          console.log('ERROR', error.message )
          return  throwError(error)
          })
        )
    }  

RESPONSE

    fetchTodos(): Observable<Todo[]>{

        let params = new HttpParams().set('_limit', '5').append('custom', 'anything')

        return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos', {params: params , observe: 'response' })
        .pipe(
          map(response =>{
            console.log(response)
            return response.body;
          }),
          delay(1500))
        .pipe( catchError( error => {
          console.log('ERROR', error.message )
          return  throwError(error)
          })
        )
    }


              body: Array(5) [ {…}, {…}, {…}, … ]
              headers: Object { normalizedNames: Map(0), lazyUpdate: null, lazyInit: lazyInit
              ok: true
              status: 200
              statusText: "OK"
              type: 4
              url: "https://jsonplaceholder.typicode.com/todos?_limit=5&custom=anything"

EVENTS

    TAP - для просмотра прмежуточных операций
    import { tap } from 'rxjs/operators';

    import { HttpEventType } from '@angular/common/http';

      removeTodo(id: number): Observable<any>{
        return  this.http.delete<void>( `https://jsonplaceholder.typicode.com/todos/${id}`, {observe: 'events'} ).pipe(
          tap( event => {
            // console.log( event )
            if ( event.type === HttpEventType.Sent){
              console.log('SENT', event)
            }
            if ( event.type === HttpEventType.Response){
              console.log('Response', event)
            }

          })
        )
      }


  // В клнсоли
      SENT Object { type: 0 }
      Response Object { headers: {…}, status: 200, statusText: "OK", url: "https://jsonplaceholder.typicode.com/todos/1", ok: true, type: 4, body: {} }


Указать ФОРМАТ ОТВЕТА

      // {responseType: 'json'}

    completeTodo(id: number): Observable<Todo>{
      return  this.http.put<Todo>(`https://jsonplaceholder.typicode.com/todos/${id}`, { completed: true}, {responseType: 'json'} )
    }


//////////////////////////////////////////////////////////////////////////////////////

ИНТЕРЦЕПТОРЫ

    auth.interceptor.ts

        import { Injectable } from '@angular/core';
        import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpEventType} from '@angular/common/http';
        import { Observable } from 'rxjs';

        @Injectable({providedIn: 'root'})
        export class AuthInterceptor implements HttpInterceptor {
         
            intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

              const cloned = req.clone( {
                headers: req.headers.append('Auth', 'Some Random Token')
              } );

              return next.handle(cloned).pipe(
                  tap( event => {
                    if (ebent,type === HttpEventType.Response ){
                      console.log('Console Interseptor response', event);
                    }
                  })
              );
            }
          }


    app.module.ts

        import { NgModule, Provider } from '@angular/core';
        import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
        import {AuthInterceptor} from './auth.interceptor';
        import { tap } from 'rxjs/operators';



        const INTERCEPTOR_PROVIDER: Provider = {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true   // чтобы интерцепторы не перезатирали друг друга
        }

        @NgModule({
        declarations: [
          AppComponent
        ],
        imports: [
          BrowserModule,
          HttpClientModule,
          FormsModule
        ],
        providers: [INTERCEPTOR_PROVIDER],    <------
        bootstrap: [AppComponent]
      })
      export class AppModule { }




****************************************************************************************

ROUTING

    Регаем набор роутов в приложении

    Создадим app/app-routing.module.ts

        import { NgModule } from '@angular/core';
        import { RouterModule, Routes } from '@angular/router';
        import { HomeComponent } from './home/home.component';
        import { AboutComponent } from './about/about.component';
        import { PostsComponent } from './posts/posts.component';

        const routes: Routes = [
          {path: '', component: HomeComponent },
          {path: 'about', component: AboutComponent },
          {path: 'posts', component: PostsComponent },
        ];

        @NgModule({
            // укажем что этот модуль необходим для роутинга
            // imports - регистрирует входящие величины для данного модуля
            // exports - открывает публичный апи к данным которые мы передаем
            imports: [ RouterModule.forRoot(routes) ],
            exports: [ RouterModule ]
        })
        export class AppRoutingModule {

        }


  app.module.ts

    import { AppRoutingModule } from './app-routing.module';

    @NgModule({
      declarations: [
        AppComponent,
        AboutComponent,
        AboutExtraComponent,
        HomeComponent,
        PostComponent,
        PostsComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule    <--------
      ],
      providers: [],
      bootstrap: [AppComponent]
    })
    export class AppModule { }


app.component.html

  Укажем место куда будем рендерить компонент

  <div class="container">
    <div class="card">
      <router-outlet></router-outlet>   <-------------
    </div>
  </div>


Активные ссылки

  Если ангулар найдет совпадение ссылки и роута, то он даст ссылке класс например active

    routerLinkActive="active"  - дает класс

    <li routerLinkActive="active"><a routerLink="/posts">Posts</a></li>

    [routerLinkActiveOptions]="{exact:true}"  - указываем что только точное совпадение . тк. /posts  совпадает со /


Программная навигация с помощью TS

    <button (click)="toPosts()" class="btn">Go to posts</button>

    >home.component.ts

        import { Router } from '@angular/router';

        constructor(private router: Router) {}

        toPosts(){
          this.router.navigate( ['/posts'] );
        }


Динамические роуты

  > app.routing.module.ts

      const routes: Routes = [
        {path: '', component: HomeComponent },
        {path: 'about', component: AboutComponent },
        {path: 'posts', component: PostsComponent },
        {path: 'posts/:id', component: PostComponent },  <----------
      ];

  >posts.component.html

      <div class="card" *ngFor="let post of postsService.posts">
        <h4>
          <a [routerLink]="['/posts', post.id]"  >   <-------------
            <strong>( ID {{post.id}})</strong>
            {{post.title}}
          </a>
        </h4>
      </div>

  >post.component.html

      <div>
        <button class="btn">Back</button>
        <h1>Title</h1>
        <p>Text</p>
        <button class="btv btn-danger">Load 4 post</button>
      </div>


Передача параметров !

>app.post.service

      export class PostsService {

        constructor() { }

        posts: Post[] = [
          {title: 'Post 1', text: 'Sample text for post 1', id: 11},
          {title: 'Post 2', text: 'Sample text for post 2', id: 22},
          {title: 'Post 3', text: 'Sample text for post 3', id: 33},
          {title: 'Post 4', text: 'Sample text for post 4', id: 44}
        ];

        getById(id: number ){
          return this.posts.find( p => p.id === id)
        }
      }

>post.component.ts

      import { Component, OnInit } from '@angular/core';
      import { ActivatedRoute, Params, Router } from '@angular/router';
      import { PostsService, Post } from '../posts.service';

      @Component({
        selector: "app-post",
        templateUrl: "./post.component.html",
        styleUrls: ["./post.component.sass"]
      })
      export class PostComponent implements OnInit {
        post: Post;

        constructor(
          private route: ActivatedRoute,
          private router: Router,
          private postService: PostsService
        ) {}

        ngOnInit() {
          this.route.params.subscribe((params: Params) => {
            this.post = this.postService.getById(+params.id);
          });
        }

        loadPost(){
          this.router.navigate( ["/posts", 44] )
        }
      }

  >post.component.html

      <div>
        <button class="btn">Back</button>
        <h1>{{post.title}}</h1>
        <p>{{post.text}}</p>
        <button class="btv btn-danger" (click)="loadPost()">Load 4 post</button>
      </div>


QueryParams


<button
  class="btn"
  [routerLink]="['/posts']"
  [queryParams]="{showIds: true}"        <-----------
  >Show Ids</button>

    import { Component, OnInit } from '@angular/core';
    import { PostsService } from '../posts.service';
    import { ActivatedRoute, Params } from '@angular/router';

    @Component({
      selector: 'app-posts',
      templateUrl: './posts.component.html',
      styleUrls: ['./posts.component.sass']
    })
    export class PostsComponent implements OnInit {

      showIds = false;           <-----------

      constructor( private postsService: PostsService, private route: ActivatedRoute) { }

      ngOnInit() {
        this.route.queryParams.subscribe( (params: Params) => {              <-----------
          console.log(params)
          this.showIds = !!params.showIds;
        })
      }

    }



    Программная навигация и QueryParams

    <button class="btn" (click)="showIdsProgremm()">Show Ids (program)</button>

      import { ActivatedRoute, Params, Router } from '@angular/router';

      constructor( private postsService: PostsService, private route: ActivatedRoute) { }

      showIdsProgremm(){
        this.router.navigate(['/posts'], { queryParams: {showIds: true}, fragment: 'count=2'  } );
      }


       ngOnInit() {
          this.route.queryParams.subscribe( (params: Params) => {
            console.log(params);
            this.showIds = !!params.showIds;
          })

          this.route.fragment.subscribe( fragment => {
            console.log(fragment)
          })
        }




Дочерние компоненты

    У нас на одном уровне лежат две компоненты

    > about
        > about.component.html
        > about.component.ts
    > about-extra
        > about-extra.component.html
        > about-extra.component.ts


> app-routing.module.ts

    import { AboutComponent } from './about/about.component';
    import { AboutExtraComponent } from './about-extra/about-extra.component';
    import { RouterModule, Routes } from '@angular/router';

    // https://localhost:4200/about/extra  ---> AboutExtraComponent
    const routes: Routes = [
      {path: '', component: HomeComponent },
      {path: 'about', component: AboutComponent , children: [       <--------
        {path: 'extra', component: AboutExtraComponent}
      ]},
      {path: 'posts', component: PostsComponent },
      {path: 'posts/:id', component: PostComponent },
    ];

    @NgModule({
        // укажем что этот модуль необходим для роутинга
        // imports - регистрирует входящие величины для данного модуля
        // exports - открывает публичный апи к данным которые мы передаем
        imports: [ RouterModule.forRoot(routes) ],
        exports: [ RouterModule ]
    })
    export class AppRoutingModule {

    }


>about.component.html

    <p>
      about page
    </p>
    <a [routerLink]="['/about', 'extra']">more...</a>

    <router-outlet></router-outlet>



Таким образом перейдя по https://localhost:4200/about/extra мы подгрузим в наш компонент about-extra


********************************************************

Обработка ошибок в роуте и редирект

    Создадим компонент страницы 404

    ng g c error-page


        <h1>ERROR 404</h1>
        <a [routerLink]="['/']">Home</a>

    app-routing.module.ts

    const routes: Routes = [
      {path: '', component: HomeComponent },
      {path: 'about', component: AboutComponent , children: [
        {path: 'extra', component: AboutExtraComponent}
      ]},
      {path: 'posts', component: PostsComponent },
      {path: 'posts/:id', component: PostComponent },
      {path: 'error', component: ErrorPageComponent },  <----------
      {path: '**', redirectTo: 'error' },               <----------
    ];


    ** -  означает все остальное 




********************************************************************

Гарды  - для защиты страницы для не залогиненого юзера


      В корне проекта создаем auth.guard.ts

            import { Injectable } from '@angular/core';
            import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
            import { Observable } from 'rxjs';
            import { AuthService } from './auth.service';

            @Injectable({providedIn: 'root'})
            export class AuthGuard implements CanActivate {

              constructor( private authService: AuthService, private router: Router){}

              canActivate(
                route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot
              ): Observable<boolean> | Promise<boolean> | boolean {
                  return this.authService.isAuthenticated().then( isAuth => {
                      if(isAuth){
                        return true
                      } else{
                          this.router.navigate(['/'], { queryParams: { auth: false} } )
                      }
                  });
              }

            }

      В корне проекта создаем auth.service.ts

            import { Injectable } from '@angular/core';

            @Injectable({providedIn: 'root'})
            export class AuthService{

                private isAuth = false;

                login(){
                  this.isAuth = true;
                }

                logout(){
                  this.isAuth = false;
                }

                isAuthenticated(): Promise<boolean>{
                  return new Promise( resolve => {
                    setTimeout( () =>{
                      resolve(this.isAuth)
                    }, 1000)
                  })
                }

            }

      В главном app.component.ts инжектируем сервис auth

          import { AuthService } from './auth.service';
          
          export class AppComponent {
            constructor(private auth: AuthService){}
          }

      В главной вьюхе app.component.html

          <li ><button class='btn' (click)="auth.login()">Login</button></li>
          <li ><button class='btn' click)="auth.logout()">Logout</button></li>


      Теперь в модуле роутинга укажем пути которые будут защищены нашим гуардом AuthGuard


      const routes: Routes = [
        {path: '', component: HomeComponent },
        {path: 'about', component: AboutComponent , children: [
          {path: 'extra', component: AboutExtraComponent}
        ]},
        {path: 'posts', component: PostsComponent, canActivate: [AuthGuard] },   <----
        {path: 'posts/:id', component: PostComponent },
        {path: 'error', component: ErrorPageComponent },
        {path: '**', redirectTo: 'error' },
      ];


      Теперь в проекте находясь на главной мы нажимаем на ссылку posts проходит время но мы на нее не переходим а в урле добавляется  http://localhost:4200/?auth=false
      Теперь нажмем на кнопу логин и чудо. Мы можем перейти на posts / Нажмем на логаут и снова не можем



  **********************************************

  Защита дочерних элементов

    Апгрейдим гуард  CanActivateChild

          import { Injectable } from '@angular/core';
          import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router,
           CanActivateChild                 <------------
            } from '@angular/router';
          import { Observable } from 'rxjs';
          import { AuthService } from './auth.service';

          @Injectable({providedIn: 'root'})
          export class AuthGuard implements CanActivate, CanActivateChild {       <------------

            constructor( private authService: AuthService, private router: Router){}

            canActivate(
              route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot
            ): Observable<boolean> | Promise<boolean> | boolean {
                return this.authService.isAuthenticated().then( isAuth => {
                    if(isAuth){
                      return true
                    } else{
                        this.router.navigate(['/'], { queryParams: { auth: false} } )
                    }
                });
            }

            canActivateChild(                         <------------
              route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot
            ): Observable<boolean> | Promise<boolean> | boolean {
                return this.canActivate(route, state)
            }

          }


    Теперь в роутере      canActivateChild: [AuthGuard]


        const routes: Routes = [
          {path: '', component: HomeComponent },
          {path: 'about', component: AboutComponent , canActivateChild: [AuthGuard], children: [
            {path: 'extra', component: AboutExtraComponent}
          ]},
          {path: 'posts', component: PostsComponent, canActivate: [AuthGuard] },
          {path: 'posts/:id', component: PostComponent },
          {path: 'error', component: ErrorPageComponent },
          {path: '**', redirectTo: 'error' },
        ];



    Все. при попытке перейти в about-extra не залогиненого кидает на главную а зщалогиненого пускает


//////////////////////////////////////////////

Модули


    @NgModule({
      declarations: [

        Тут регистрируем компоненты директивы и пайпы чтобы ангуляр знал о них
        и мог использовать

        AppComponent,
        HomePageComponent,
        AboutPageComponent,
        AboutExtraPageComponent,
        ColorDirective,
        PageNamePipe
      ],
      imports: [

        Импортируем другие модули

        BrowserModule,
        FormsModule,
        AppRoutingModule,
      ],
      providers: [

         тут регистрируем сервисы

      ],
      сюда передаем главный компонент приложения
      bootstrap: [AppComponent]
    })
    export class AppModule {
    }



//////////////////////////////////////////////////////////////////////////

Динамические компоненты 

    Это компоненты создаваемые через ts

    > app.component.html

          // тут будет модалка
          <ng-template appRef> </ng-template>

          <nav class='navbar'>
            <h1>ADC</h1>
          </nav>

          <div class="container">
            <div class="card">
              <h1>S content</h1>
              <button class="btn" (click)="showModal()">Show modal</button>
            </div>
          </div>

    > app.comnponent.ts

          import { Component, ComponentFactoryResolver, ViewChild } from '@angular/core';
          import { ModalComponent } from './modal/modal.component';
          import { RefDirective } from './ref.directive';

          @Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
          })
          export class AppComponent {

            @ViewChild(RefDirective, {static: false}) refDir: RefDirective;

            /**
            * @param resolver;
            */
            constructor(private resolver: ComponentFactoryResolver ){

            }

            showModal(){
              // Фабрика которая создает компонент ModalComponent
              const modalFactory  = this.resolver.resolveComponentFactory(ModalComponent);
              // Для отображения нового созданного компонента на странице используем
              // очистка контейнера
              this.refDir.containerRef.clear();
              const component = this.refDir.containerRef.createComponent(modalFactory);

              component.instance.title = 'DYN TITLE'

              component.instance.close.subscribe( () => {
                this.refDir.containerRef.clear();
              })

            }

          }

    > modal.component.ts

          import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

            @Component({
              selector: 'app-modal',
              templateUrl: './modal.component.html',
              styleUrls: ['./modal.component.scss']
            })
            export class ModalComponent implements OnInit {

            @Input() title = 'default';
            @Output() close  = new EventEmitter<void>();

              constructor() { }

              ngOnInit() {
              }

            }

    > modal.component.html

        <div class="backdrop"></div>

          <div class="modal">

            <nav class="navbar">
              {{title}}
            </nav>

            <p>Lorem ipsum dolor sit amet.</p>

            <button class="btn" (click)="close.emit()">Close</button>
        </div>

    > app.module.ts


        import { BrowserModule } from '@angular/platform-browser';
        import { NgModule } from '@angular/core';

        import { AppComponent } from './app.component';
        import { ModalComponent } from './modal/modal.component';
        import { RefDirective } from './ref.directive';

        @NgModule({
          declarations: [
            AppComponent,
            ModalComponent,
            RefDirective
          ],
          imports: [
            BrowserModule
          ],
          providers: [],
          entryComponents: [ModalComponent],
          bootstrap: [AppComponent]
        })
        export class AppModule { }

  > ref.directive.ts

      import { Directive, ViewContainerRef } from "@angular/core";

      @Directive({
        selector: '[appRef]'
      })
      export class RefDirective{

        constructor(public containerRef: ViewContainerRef){}

      }




*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

UNIT TESTINGS

Для примера создадим папку playground

    > compute.ts

        export function compute(x){
            if( X < 0 ){
                return 0;
            }

            return x + 1
        }

    > compute.spec.ts

        import { compute } from './compute';

        describe('compute', () => { 

            it('should return 0 if negative input', () => {
                // передаем -1
                const result = compute(-1)
                // ожилаем получить 0
                expect(result).toBe(0)
            })
              it('should imcrement input if positive ', () => {
                  // передаем 1
                  const result = compute(1)
                  // ожилаем получить 2
                  expect(result).toBe(2)
              })
        })


    в package.json есть команда 

            "test": "ng test",


    Chrome 70.0.3538 (Linux 0.0.0): Executed 5 of 5 SUCCESS (1.514 secs / 1.475 secs)
    TOTAL: 5 SUCCESS
    TOTAL: 5 SUCCESS



TESTING STRING AND ARRAY

    > greet.ts

        export function greet(name) {
            return 'Hello my dear ' + name;
        }


    > greet.spec.ts

        import { greet } from './greet';

        describe('greet', () => {
            it('should include name in return message', () => {
                // toBe = должно быть полное соответствие 
                /* expect(greet('Denis')).toBe('Hello Denis'); */
                // toContain - не строгое. Проcто должна входить
                expect(greet('Denis')).toContain('Denis');
            })
        } )


        10% building 2/2 modules 0 active16 12 2019 07:52:21.155:WARN [karma]: No captured browser, open http://localhost:9876/
        16 12 2019 07:52:21.549:INFO [karma-server]: Karma v4.1.0 server started at http://0.0.0.0:9876/
        16 12 2019 07:52:21.550:INFO [launcher]: Launching browsers Chrome with concurrency unlimited
        16 12 2019 07:52:21.574:INFO [launcher]: Starting browser Chrome
        16 12 2019 07:52:28.064:WARN [karma]: No captured browser, open http://localhost:9876/
        16 12 2019 07:52:28.255:INFO [Chrome 70.0.3538 (Linux 0.0.0)]: Connected on socket _7RTqYJ7hQp_B_D3AAAA with id 78723521
        Chrome 70.0.3538 (Linux 0.0.0): Executed 3 of 3 SUCCESS (0.03 secs / 0.004 secs)
        TOTAL: 3 SUCCESS
        TOTAL: 3 SUCCESS


  Array

      > array.ts

        export function countries() {
          return ['ru', 'ua', 'uk'];
        }

      > array.spec.ts

        import { countries } from './array';

        describe("countries", () => {
          it("should contaim counties codes", () => {
              const result = countries();

              expect(result).toContain('ru')
              expect(result).toContain('ua')
              expect(result).toContain('ue')   <---- тут не существующий
          });
        });



        Error: Expected [ 'ru', 'ua', 'uk' ] to contain 'ue'.
            at <Jasmine>
            at UserContext.it (http://localhost:9876/_karma_webpack_/src/app/playground/array.spec.ts:9:22)
            at ZoneDelegate.invoke (http://localhost:9876/_karma_webpack_/node_modules/zone.js/dist/zone-evergreen.js:359:1)
Chrome 70.0.3538 (Linux 0.0.0): Executed 4 of 4 (1 FAILED) (0.176 secs / 0.146 secs)
TOTAL: 1 FAILED, 3 SUCCESS
TOTAL: 1 FAILED, 3 SUCCESS


Тестирование компонента.

    Создадим простой компонент и зарегаем его в модуле

          import { Component, OnInit } from '@angular/core';

          @Component({
            selector: 'app-counter',
            template: `Counter {{ counter }}`
          })
          export class CounterComponent {
            counter = 0;

            increment(){
              this.counter++
            }

            decrement(){
              this.counter--
            }

          }

    > counter.component.sprc.ts


          import { CounterComponent } from './counter.component';

          // Пропустить весь тест  xdescribe
          describe('CounterComponent', () => {

                let component: CounterComponent;
                // beforeEach - выполнить какоето действие перед каждым it
                //              например будем создавать компонент как новый instance
                // beforeAll -  бедет вызван перед всеми it
                // afterEach - будет вызван после завершения каждого it 
                // afterAll - будет вызван после всех it 
                beforeEach( () => {
                  component = new CounterComponent();
                });

                it('should uncrement counter by 1', () => {
                    component.increment();
                    expect(component.counter).toBe(1);
                });

                // пропустить пункт теста  Пишем xit
                xit('should decrement counter by 1', () => {
                    component.decrement();
                    expect(component.counter).toBe(-1);
                });

                it('should decrement counter by 1', () => {
                    component.decrement();
                    expect(component.counter).toBe(-1);
                });

          });

    ng test

    Chrome 70.0.3538 (Linux 0.0.0): Executed 6 of 7 (skipped 1) SUCCESS (0.025 secs / 0.01 secs)
    TOTAL: 6 SUCCESS
    TOTAL: 6 SUCCES


Тестирование EventEmitter

            import { Component, OnInit, Output } from '@angular/core';
            import { EventEmitter } from '@angular/core';

            @Component({
              selector: 'app-counter',
              template: `Counter {{ counter }}`
            })
            export class CounterComponent {
              counter = 0;

              @Output()  counterEmitter = new EventEmitter<number>();    <-----------------

              increment(){
                this.counter++;
              this.counterEmitter.emit(this.counter);                   <-----------------
              }

              decrement(){
                this.counter--;
              }

            }


      > counter.component.sprc.ts

            import { CounterComponent } from './counter.component';

            // Пропустить весь тест  xdescribe
            describe('CounterComponent', () => {

                  let component: CounterComponent;
                  // beforeEach - выполнить какоето действие перед каждым it
                  //              например будем создавать компонент как новый instance
                  // beforeAll -  бедет вызван перед всеми it
                  // afterEach - будет вызван после завершения каждого it 
                  // afterAll - будет вызван после всех it 
                  beforeEach( () => {
                    component = new CounterComponent();
                  });

                  it('should uncrement counter by 1', () => {
                      component.increment();
                      expect(component.counter).toBe(1);
                  });

             

                  it("should increment value by event emitter", () => {    <-----------------
                    let result = null;
                    component.counterEmitter.subscribe( value => {
                      result = value;
                    });
                    component.increment();
                    expect(result).toBe(1);
                  });

            });


Тестирование форм

    >counter.component.ts

        import { Component, OnInit, Output } from '@angular/core';
        import { FormGroup, FormBuilder, Validators } from '@angular/forms';

        @Component({
          selector: 'app-counter',
          template: `Counter {{ counter }}`
        })
        export class CounterComponent {

          public form: FormGroup;

          constructor(private fb: FormBuilder){
            this.form = fb.group( {
              login: ['', Validators.required],
              email: ['']
            } );
          }

        }

    > app.module.ts

        import { FormsModule } from "@angular/forms";

        imports: [
            FormsModule
          ],

    > counter.spec.ts


              import { CounterComponent } from './counter.component';
              import { FormBuilder } from '@angular/forms';

              // Пропустить весь тест  xdescribe
              describe('CounterComponent', () => {

                    let component: CounterComponent;
                    // beforeEach - выполнить какоето действие перед каждым it
                    //              например будем создавать компонент как новый instance
                    // beforeAll -  бедет вызван перед всеми it
                    // afterEach - будет вызван после завершения каждого it 
                    // afterAll - будет вызван после всех it 
                    beforeEach( () => {
                      component = new CounterComponent(new FormBuilder());   <------------
                    });

                    //form

                    // Протестируем что контролы были созданы

                    it('should creat form with 2 controls', () => {
                      expect(component.form.contains('login')).toBe(true)
                      expect(component.form.contains('email')).toBeTruthy()
                    });

                    // тест на работу валидаторов

                    it("should mark login as invalid if empty value", () => {

                      const control = component.form.get('login');

                      control.setValue('');

                      expect(control.valid).toBeFalsy();
                      
                    });

              });


Тестирование сервиса

        import { PostsComponent } from "./posts.component";
        import { PostsService } from './posts.service';
        import { EMPTY, throwError } from 'rxjs';
        import { of } from 'rxjs';

        describe('PostsComponent', () => {

          let component: PostsComponent;
          let service: PostsService;

          beforeEach( () => {
            service = new PostsService(null);
            component = new PostsComponent(service);
          })

          // протестим у сервиса метод fetch и что загрузка пошла
          it('it should call fetch when ngOnInit', () => {
            // spyOn - метод шпионит за вызовами сервиса 
            const spy = spyOn(service, 'fetch').and.callFake( () => {
              return EMPTY;
            });

            component.ngOnInit()

            expect(spy).toHaveBeenCalled()
          });


          // проверим на данные 
          it('it should update posts lenght after ngOnInit', () => {
            const posts = [1, 2, 3, 4];
            // spyOn - метод шпионит за вызовами сервиса
            spyOn(service, "fetch").and.returnValue( of(posts) ) ;

            component.ngOnInit();

            expect(component.posts.length).toBe(posts.length);
          })

          // проверим метод add
          it('it should add new post', () => {

          const post = {title: 'test'}
          const spy = spyOn(service, 'create').and.returnValue(of(post));

          component.add(post.title);

          expect(spy).toHaveBeenCalled();
          expect(component.posts.includes(post)).toBeTruthy();

          });


          // проверим на ошибку
        // tslint:disable-next-line: align
        it('it should catch error', () => {
          const error = 'Error message'
          spyOn(service, 'create').and.returnValue(throwError(error));

          component.add('Post Title');

          expect(component.message).toBe(error)

        });



          // delete
            it('it should remove post if user confirms', () => {

              const spy = spyOn(service, 'remove').and.returnValue(EMPTY);

              spyOn(window, "confirm").and.returnValue(true);

              component.delete(10);

              expect(spy).toHaveBeenCalledWith(10);
          });

          // cancel delete
            it('it should*not remove post if user doesnt confirm', () => {

              const spy = spyOn(service, 'remove').and.returnValue(EMPTY);

              spyOn(window, "confirm").and.returnValue(false);

              component.delete(10);

              expect(spy).not.toHaveBeenCalled();
          });



        })

УЗНАТЬ ПРОЦЕНТ ПОКРЫТИЯ ЮНИТ ТЕСТАМИ

      ng test --code-coverage


*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**/*/*

Интеграционное тестирование

    Например протестировать взаимодействие  компонента и шаблона

    Настроим интеграционные тесты


          import {CounterComponent} from "./counter.component";
          import { TestBed, ComponentFixture } from '@angular/core/testing';

          describe('CounterComponent', () => {
            let component: CounterComponent;

            let fixture: ComponentFixture<CounterComponent>;

            beforeEach(() => {
                TestBed.configureTestingModule({
                  declarations: [CounterComponent]
                })

                fixture = TestBed.createComponent(CounterComponent);

                // получить сам компонент
                component = fixture.componentInstance;
            })


            it(' should be created', () => {
                expect(component).toBeDefined();
            })

          })


  --------------------------------------------------------------------------------------------

  Есть каунтер . Проверим что если он изменился в компоненте то он корректно отобразится в шаблоне

      import { By } from "@angular/platform-browser";

        it(" should render count property", () => {
        // зададим значение в компоненте
        component.counter = 42;
        // отследим изменение 
        fixture.detectChanges();

        let debugElem = fixture.debugElement.query(By.css('.counter'));
        let el: HTMLElement = debugElem.nativeElement;

        expect(el.textContent).toContain('42'.toString())

      });


      проверка на изменение класса

          <h1 class="counter" [class.green]="counter % 2 === 0">{{counter}}</h1>

          it(" should add green class", () => {
           
            component.counter = 6;
            // отследим изменение 
            fixture.detectChanges();

            let debugElem = fixture.debugElement.query(By.css('.counter'));
            let el: HTMLElement = debugElem.nativeElement;

            expect(el.classList.contains('green')).toBeTruthy()

          });






Анимации

  Web Animations API

      "@angular/animations": "~8.2.7",  package.json

  В модуле 


    import { BrowserModule } from '@angular/platform-browser';
    import { BrowserAnimationsModule } from '@angular/platform-browser/animations';  <--------
    import { NgModule } from '@angular/core';

    import { AppComponent } from './app.component';

    @NgModule({
      declarations: [
        AppComponent
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule       <--------
      ],
      providers: [],
      bootstrap: [AppComponent]
    })
    export class AppModule { }



    В компоненте

    import { Component } from '@angular/core';
                       import { Component } from '@angular/core';
import {
  trigger,
  state,
  style,
  transition,
  animate,
  group,
  keyframes,
} from "@angular/animations";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  animations: [
    trigger("mybox", [
      state("start", style({ background: "blue" })),
      state("end", style({ background: "red", transform: "scale(1.2)" })),
      state(
        "special",
        style({
          background: "orange",
          transform: "scale(0.5)",
          borderRadius: "50%"
        })
      ),
      transition("start => end", animate(450)),
      transition("end => start", animate("800ms ease-in-out")),
      /*       transition('special <=> *', animate(500)), */
      transition("special <=> *", [
        style({ background: "green" }),
        animate("1s", style({ background: "pink" })),
        animate(750)
      ]),
      // для плавного появления скрытия
      // "void => *
      transition(":enter", [
        /*  style({ opacity: 0 }),
        animate("850ms ease-out") */
        animate(
          "3s",
          keyframes([
            style({ background: "red", offset: 0 }),
            style({ background: "orange", offset: 0.3 }),
            style({ background: "green", offset: 0.6 }),
            style({ background: "blue", offset: 1 })
          ])
        )
      ]),
      // * => void
      transition(":leave", [
        style({ opacity: 1 }),
        group([
          animate(750, style({ opacity: 0, transform: "scale(1.2)" })),
          animate(300, style({ color: "lime", fontWeight: "bold" }))
        ])
      ])
    ])
  ]
})
export class AppComponent {
  // переменная состояния нужного нам элемента
  boxState = "start";
  visible = true;

  animate() {
    this.boxState = this.boxState === "start" ? "end" : "start";
  }
}


<button (click)="animate()">Animate</button>
<button (click)="boxState = 'special' ">Set special State</button>
<button (click)="visible = !visible ">Visible</button>
<hr/>

<div class="wrap">
  <div 
    class="box"
    [@mybox] = "boxState"
    *ngIf="visible"
  >
  <h4> {{ boxState | uppercase}} </h4>
</div>
</div>



Библиотека NG-ANIMATE

      ng animate on npm.js

      npm install ng-animate --save




!!!!!!!!!!!!!!!!!!!!! СБОРКА ДЛЯ ПРОДА  !!!!!!!!!!!!!!!!!!!!!!!!

ng build --prod

Проверить можно с помощью http-сервера

npm i -g http-server

cd dist/angular-blog

> http-server -p 4201


!!!!!!!!!!!!!!!!!!!! Разворачивание приложения !!!!!!!!!!!!!!!!!!!!!

firebase - > console - > HOSTING

anton@Zanton-pc:/var/www/homeworks/angular-blog/dist/angular-blog$ npm install -g firebase-tools

firebase login


перейдем в корневую папку проекта angular-blog

firebase init

    ◯ Database: Deploy Firebase Realtime Database Rules
    ◯ Firestore: Deploy rules and create indexes for Firestore
    ◯ Functions: Configure and deploy Cloud Functions
    ❯◉ Hosting: Configure and deploy Firebase Hosting sites
    ◯ Storage: Deploy Cloud Storage security rules
    ◯ Emulators: Set up local emulators for Firebase features



    ? Please select an option: (Use arrow keys)
    ❯ Use an existing project 
      Create a new project 
      Add Firebase to an existing Google Cloud Platform project 
      Don't set up a default project 



    ❯ angular-blog-d6679 (angular-blog) 

    ? What do you want to use as your public directory? dist/angular-blog

    ? Configure as a single-page app (rewrite all urls to /index.html)? Yes
    ? File dist/angular-blog/index.html already exists. Overwrite? No 


    firebase deploy

    Все. Приложение доступно по домену  

    https://angular-blog-d6679.firebaseapp.com/
    https://angular-blog-d6679.web.app/