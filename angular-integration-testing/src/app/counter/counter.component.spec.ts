import { CounterComponent } from "./counter.component";
import { TestBed, ComponentFixture } from "@angular/core/testing";
import { By } from "@angular/platform-browser";

describe("CounterComponent", () => {
  let component: CounterComponent;

  let fixture: ComponentFixture<CounterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CounterComponent]
    });

    fixture = TestBed.createComponent(CounterComponent);

    // получить сам компонент
    component = fixture.componentInstance;
  });

  it(" should be created", () => {
    expect(component).toBeDefined();
  });

  it(" should render count property", () => {
    // зададим значение в компоненте
    component.counter = 42;
    // отследим изменение 
    fixture.detectChanges();

    let debugElem = fixture.debugElement.query(By.css('.counter'));
    let el: HTMLElement = debugElem.nativeElement;

    expect(el.textContent).toContain('42'.toString())

  });
});
